<?xml version="1.0" encoding="UTF-8"?>
<indicatorTemplateXML>
	<author>Colcamex Resources Inc. for Oscar EMR</author>
	<uid></uid>
	<heading>
		<category>Preventive Health Care</category>
		<subCategory>OMD-Immunization</subCategory>
		<name>OMD-Patient Care: Childhood Immunization Recall</name>
		<metricSetName>OntarioMD Childhood Immunization Recall</metricSetName>
		<metricLabel>Incomplete</metricLabel>
		<definition>% of patients aged 30 to 42 months who have received all of the ministry-supplied immunizations as recommended by the National Advisory Committee on immunizations.</definition>
		<framework>Based on and adapted from AFHTO D2D 3.0 Indicators: Data Dictionary version 4 (Nov 2015)</framework>
		<frameworkVersion>02-08-2018</frameworkVersion>
		<notes></notes>
	</heading>
	<indicatorQuery>
		<version>02-08-2018</version>
		<params>
			<!-- 
				Required parameter provider. Value options are: 
					[ an individual providerNo, or provider range ] ie: value="370, 1001" 
					"all" ie: value="all" including null.
					"loggedInProvider" ie:
				Default is "loggedInProvider"
				Use this parameter in the query as ${provider}
				This parameter should be used for fetching patient's assigned to a MRP.
				ie: WHERE demographic.provider_no = ${provider}
			-->
			<parameter id="provider" name="provider_no" value="loggedInProvider" />
			<parameter id="pstatus" name="Patient Status" value="'AC'" />	
			<parameter id="dTaPIPVHib" name="DTaPIPVHib" value="'DTaP-IPV-Hib'" />
			<parameter id="pneumococcal" name="Pneumococcal" value="'Pneu-C'" />
			<parameter id="rotavirus" name="Rotavirus" value="'Rot'" />
			<parameter id="meningococcal" name="Meningococcal" value="'MenC-C'" />
			<parameter id="measlesMumpsRubella" name="MeaslesMumpsRubella" value="'MMR'" />
			<parameter id="varicella" name="Varicella" value="'VZ'" />
			
		</params>
		<range>
<!--
			<upperLimit id="age" label="Max Age Months" name="Age" value="2.41667" />
			<lowerLimit id="age" label="Min Age Months" name="Age" value="1.5" />
-->						
			<upperLimit id="age" label="Max Age Months" name="Age" value="29" />
                        <lowerLimit id="age" label="Min Age Months" name="Age" value="18" />

			<upperLimit id="date" label="Date Today" name="Date" value="NOW()" />
            <lowerLimit id="date" label="12 Months" name="Date" value="DATE_SUB( NOW(), INTERVAL 12 MONTH )" />

		</range>
		<query>
			<!-- Indicator SQL Query here -->
			SELECT 
				-- count patients that satisfy all 5 requirements
				IF( COUNT(fin.patient) > 0,
					SUM(
						IF( fin.dtap >= 4  
						AND fin.pneu >= 3  
						AND fin.menc >= 1
						AND fin.mmr >= 1
						AND fin.vz >= 1
						, 1, 0 )
					) 
				, 0 ) AS '% Complete',
			
				-- the remainder missed one of the 5 requirements.
				IF( COUNT(fin.patient) > 0,					
					SUM(
                                                IF( (4 > fin.dtap AND fin.dtap_r = 0 AND ( fin.pneu >= 3 OR fin.pneu_r = 0) AND (fin.menc >= 1 OR fin.menc_r = 0) AND (fin.mmr >= 1 OR fin.mmr_r = 0 ) AND (fin.vz >= 1 OR fin.vz_r = 0) )
                                                OR ( 3 > fin.pneu AND fin.pneu_r = 0 AND (fin.dtap >= 4 OR fin.dtap_r = 0) AND (fin.menc >= 1 OR fin.menc_r = 0) AND (fin.mmr >= 1 OR fin.mmr_r = 0 ) AND (fin.vz >= 1 OR fin.vz_r = 0) )
                                                OR ( 1 > fin.menc AND fin.menc_r = 0 AND (fin.pneu >= 3 OR fin.pneu_r = 0) AND (fin.dtap >= 4 OR fin.dtap_r = 0) AND (fin.mmr >= 1 OR fin.mmr_r = 0 ) AND (fin.vz >= 1 OR fin.vz_r = 0) )
                                                OR ( 1 > fin.mmr AND fin.mmr_r = 0 AND (fin.menc >= 1 OR fin.menc_r = 0) AND (fin.pneu >= 3 OR fin.pneu_r = 0 ) AND (fin.dtap >= 4 OR fin.dtap_r = 0) AND (fin.vz >=1 OR fin.vz_r = 0) )
                                                OR ( 1 > fin.vz AND fin.vz_r = 0 AND (fin.mmr >= 1 OR fin.mmr_r = 0) AND (fin.menc >= 1 OR fin.menc_r = 0 ) AND (fin.pneu >= 3 OR fin.pneu_r = 0 ) AND (fin.dtap >= 4 OR fin.dtap_r = 0 ) )

						, 1, 0 )
					) 
				, 0 ) AS '% Incomplete',
				
				IF( COUNT(fin.patient) > 0,
					SUM(
						IF( ( 4 > fin.dtap AND fin.dtap_r > 0) 
						OR ( 3 > fin.pneu AND fin.pneu_r > 0) 
						OR ( fin.menc = 0 AND fin.menc_r > 0)
						OR ( fin.mmr = 0 AND fin.mmr_r > 0) 
						OR ( fin.vz = 0 AND fin.vz_r > 0)
						, 1, 0 )
					) 
				, 0 )AS '% Declined one or more'
			
			FROM (
				SELECT  
					d.demographic_no AS 'patient',
					IFNULL( DTaP.id, 0 ) AS 'dtap',
					IFNULL( DTaP_r.id, 0 ) AS 'dtap_r',
					IFNULL( PNEU.id, 0 ) AS 'pneu',
					IFNULL( PNEU_r.id, 0 ) AS 'pneu_r',
					IFNULL( MC.id, 0 ) AS 'menc',
					IFNULL( MC_r.id, 0 ) AS 'menc_r',
					IFNULL( MM.id, 0 ) AS 'mmr',
					IFNULL( MM_r.id, 0 ) AS 'mmr_r',
					IFNULL( VZ.id, 0 ) AS 'vz',
					IFNULL( VZ_r.id, 0 ) AS 'vz_r'
					
				FROM demographic d
			
				-- 4 instances of - DTaP-IPV-Hib - Diphtheria, Tetanus, Pertussis, Polio, Haemophilus influenzae type B
				LEFT JOIN ( 
					SELECT p.demographic_no, COUNT(p.id) AS id
					FROM preventions p		 
					WHERE p.prevention_type = ${dTaPIPVHib}
					AND p.deleted = 0
					AND p.refused = 0
					GROUP BY p.demographic_no 
				) DTaP
				ON (d.demographic_no = DTaP.demographic_no)
			
				-- WITH  a refusal of any of the following - DTaP-IPV-Hib,  Pneumococcal conjugate,  Meningococcal Conjugate C,  Measles, Mumps, Rubella or Var  - Varicella
				-- in the past 12 months
				-- refused in the past 12 months
                                LEFT JOIN (
                                        SELECT p.demographic_no, COUNT(p.id) AS id
                                        FROM preventions p
                                        WHERE p.prevention_type = ${dTaPIPVHib}
                                        AND p.deleted = 0
                                        AND p.refused = 1
                                        AND p.prevention_date > ${lowerLimit.date}
                                        GROUP BY p.demographic_no
                                ) DTaP_r
                                ON (d.demographic_no = DTaP_r.demographic_no)

				
				-- 3 instances of - Pneu-C-13  - Pneumococcal Conjugate 13
				LEFT JOIN ( 
					SELECT p.demographic_no, COUNT(p.id) AS id
					FROM preventions p		 
					WHERE p.prevention_type = ${pneumococcal}
					AND p.deleted = 0
					AND p.refused = 0
					GROUP BY p.demographic_no 
				) PNEU
				ON (d.demographic_no = PNEU.demographic_no)
			
				LEFT JOIN (
                                        SELECT p.demographic_no, COUNT(p.id) AS id
                                        FROM preventions p
                                        WHERE p.prevention_type = ${pneumococcal}
                                        AND p.deleted = 0
                                        AND p.refused = 1
					AND p.prevention_date > ${lowerLimit.date}
                                        GROUP BY p.demographic_no
                                ) PNEU_r
                                ON (d.demographic_no = PNEU_r.demographic_no)

				-- 1 instance of - Men-C-C  - Meningococcal Conjugate 
				LEFT JOIN ( 
					SELECT p.demographic_no, COUNT(p.id) AS id
					FROM preventions p		 
					WHERE p.prevention_type = ${meningococcal}
					AND p.deleted = 0
					AND p.refused = 0
					GROUP BY p.demographic_no
				) MC
				ON (d.demographic_no = MC.demographic_no)
			
				LEFT JOIN (
                                        SELECT p.demographic_no, COUNT(p.id) AS id
                                        FROM preventions p
                                        WHERE p.prevention_type = ${meningococcal}
                                        AND p.deleted = 0
                                        AND p.refused = 1
					AND p.prevention_date > ${lowerLimit.date}
                                        GROUP BY p.demographic_no
                                ) MC_r
                                ON (d.demographic_no = MC_r.demographic_no)

				
				-- 1 instance of - MMR  - Measles, Mumps, Rubella
				LEFT JOIN ( 
					SELECT p.demographic_no, COUNT(p.id) AS id
					FROM preventions p		 
					WHERE p.prevention_type = ${measlesMumpsRubella}
					AND p.deleted = 0
					AND p.refused = 0
					GROUP BY p.demographic_no
				) MM
				ON (d.demographic_no = MM.demographic_no)
			
				LEFT JOIN (
                                        SELECT p.demographic_no, COUNT(p.id) AS id
                                        FROM preventions p
                                        WHERE p.prevention_type = ${measlesMumpsRubella}
                                        AND p.deleted = 0
                                        AND p.refused = 1
					AND p.prevention_date > ${lowerLimit.date}
                                        GROUP BY p.demographic_no
                                ) MM_r
                                ON (d.demographic_no = MM_r.demographic_no)

				-- 1 instance of - Var  - Varicella
				LEFT JOIN ( 
					SELECT p.demographic_no, COUNT(p.id) AS id
					FROM preventions p		 
					WHERE p.prevention_type = ${varicella}
					AND p.deleted = 0
					AND p.refused = 0
					GROUP BY p.demographic_no
				) VZ
				ON (d.demographic_no = VZ.demographic_no)
				
				LEFT JOIN (
                                        SELECT p.demographic_no, COUNT(p.id) AS id
                                        FROM preventions p
                                        WHERE p.prevention_type = ${varicella}
                                        AND p.deleted = 0
                                        AND p.refused = 1
					AND p.prevention_date > ${lowerLimit.date}
                                        GROUP BY p.demographic_no
                                ) VZ_r
                                ON (d.demographic_no = VZ_r.demographic_no)

			
				-- PATIENTS BETWEEN 30 TO 42 MONTHS OLD
				WHERE d.patient_status = ${pstatus}
				AND d.provider_no = '${provider}'
				AND d.demographic_no > 0 
				AND CURRENT_DATE() >= DATE_ADD(DATE( CONCAT(d.year_of_birth,"-",d.month_of_birth,"-",d.date_of_birth) ), INTERVAL ${lowerLimit.age} MONTH)
                        	AND DATE_ADD(DATE( CONCAT(d.year_of_birth,"-",d.month_of_birth,"-",d.date_of_birth) ), INTERVAL ${upperLimit.age} MONTH) >= CURRENT_DATE()
				
			) fin;
		</query>
	</indicatorQuery>
	<drillDownQuery>
		<version>02-08-2018</version>
		<params>
			<parameter id="provider" name="provider_no" value="loggedInProvider" />
			<parameter id="pstatus" name="Patient Status" value="'AC'" />	
			<parameter id="dTaPIPVHib" name="DTaPIPVHib" value="'DTaP-IPV-Hib'" />
			<parameter id="pneumococcal" name="Pneumococcal" value="'Pneu-C'" />
			<parameter id="rotavirus" name="Rotavirus" value="'Rot'" />
			<parameter id="meningococcal" name="Meningococcal" value="'MenC-C'" />
			<parameter id="measlesMumpsRubella" name="MeaslesMumpsRubella" value="'MMR'" />
			<parameter id="varicella" name="Varicella" value="'VZ'" />

			<parameter id="sharedMetricLabel" name="sharedMetricLabel" value="sharedMetricLabel" />
		</params>
		<range>
<!--
			<upperLimit id="age" label="Max Age Months" name="Age" value="2.41667" />
			<lowerLimit id="age" label="Min Age Months" name="Age" value="1.5" />
-->
			<upperLimit id="age" label="Max Age Months" name="Age" value="29" />
                        <lowerLimit id="age" label="Min Age Months" name="Age" value="18" />
	
			<upperLimit id="date" label="Date Today" name="Date" value="NOW()" />
            <lowerLimit id="date" label="12 Months" name="Date" value="DATE_SUB( NOW(), INTERVAL 12 MONTH )" />

		</range>
		<displayColumns>
			<column id="demographic" name="d.demographic_no" title="Patient Id" primary="true" />
			<column id="name" name="CONCAT( d.last_name, ', ', d.first_name )" title="Patient Name" primary="false" />
			<column id="dob" name="DATE_FORMAT( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth), '%m-%d-%Y' )" title="Patient DOB (mm-dd-yyyy)" primary="false" />
			<column id="age" name="ROUND( ABS(DATEDIFF( DATE( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth) ), NOW() ) / 365.25), 1)" title="Patient Age" primary="false" />
			<column id="phonenumber" name="IFNULL( d.phone, '')" title="Patient Phone Number" primary="false" />
			
			<column id="dtapcount" name="IFNULL( DTaP.id, '' )" title="Count of DTaP instances" primary="false" />
<!--			<column id="dtaprefused" name="IFNULL( REF_dtap.refused, 'No' )" title="DTaP Refused Status" primary="false" />
-->			<column id="pneucount" name="IFNULL( PNEU.id, 0 )" title="Count of Pneumococcal instances" primary="false" />
<!--			<column id="pneurefused" name="IFNULL( REF_pneu.refused, 'No' )" title="Pneumococcal Refused Status" primary="false" />
-->			<column id="mencount" name="IFNULL( MC.id, 0 )" title="Count of Meningococcal Instances" primary="false" />
<!--			<column id="menrefused" name="IFNULL( REF_men.refused, 'No' )" title="Meningococcal Refused Status" primary="false" />
-->			<column id="mmrcount" name="IFNULL( MM.id, 0 )" title="Count of MMR instances" primary="false" />
<!--			<column id="mmrrefused" name="IFNULL( REF_mmr.refused, 'No' )" title="MMR Refused Status" primary="false" />
-->			<column id="vzcount" name="IFNULL( VZ.id, 0 )" title="Count of Varicella instances" primary="false" />
<!--			<column id="vzrefused" name="IFNULL( REF_vz.refused, 'No' )" title="Varicella Refused Status" primary="false" />
-->			<column id="rotcount" name="IFNULL( ROT.id, 0 )" title="Count of Rotovirus instances" primary="false" />
<!--			<column id="rotrefused" name="IFNULL( REF_rot.refused, 'No' )" title="Rotovirus Refused Status" primary="false" />
-->

		</displayColumns>
		<exportColumns>
			<column id="demographic" name="d.demographic_no" title="Patient Id" primary="true" />
			<column id="name" name="CONCAT( d.last_name, ', ', d.first_name )" title="Patient Name" primary="false" />
			<column id="dob" name="DATE_FORMAT( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth), '%m-%d-%Y' )" title="Patient DOB (mm-dd-yyyy)" primary="false" />
			<column id="age" name="ROUND( ABS(DATEDIFF( DATE( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth) ), NOW() ) / 365.25), 1)" title="Patient Age" primary="false" />
			<column id="phonenumber" name="IFNULL( d.phone, '')" title="Patient Phone Number" primary="false" />
			<column id="dtapcount" name="IFNULL( DTaP.id, '' )" title="Count of DTaP instances" primary="false" />
<!--                    <column id="dtaprefused" name="IFNULL( REF_dtap.refused, 'No' )" title="DTaP Refused Status" primary="false" />
-->                     <column id="pneucount" name="IFNULL( PNEU.id, 0 )" title="Count of Pneumococcal instances" primary="false" />
<!--                    <column id="pneurefused" name="IFNULL( REF_pneu.refused, 'No' )" title="Pneumococcal Refused Status" primary="false" />
-->                     <column id="mencount" name="IFNULL( MC.id, 0 )" title="Count of Meningococcal Instances" primary="false" />
<!--                    <column id="menrefused" name="IFNULL( REF_men.refused, 'No' )" title="Meningococcal Refused Status" primary="false" />
-->                     <column id="mmrcount" name="IFNULL( MM.id, 0 )" title="Count of MMR instances" primary="false" />
<!--                    <column id="mmrrefused" name="IFNULL( REF_mmr.refused, 'No' )" title="MMR Refused Status" primary="false" />
-->                     <column id="vzcount" name="IFNULL( VZ.id, 0 )" title="Count of Varicella instances" primary="false" />
<!--                    <column id="vzrefused" name="IFNULL( REF_vz.refused, 'No' )" title="Varicella Refused Status" primary="false" />
-->                     <column id="rotcount" name="IFNULL( ROT.id, 0 )" title="Count of Rotovirus instances" primary="false" />
<!--                    <column id="rotrefused" name="IFNULL( REF_rot.refused, 'No' )" title="Rotovirus Refused Status" primary="false" />
-->
		</exportColumns>
		<query>
			<!-- Drilldown SQL Query here -->				
			SELECT  
			*
			FROM demographic d
		
			-- 4 instances of - DTaP-IPV-Hib - Diphtheria, Tetanus, Pertussis, Polio, Haemophilus influenzae type B
			LEFT JOIN ( 
				SELECT p.demographic_no, COUNT(p.id) AS id
				FROM preventions p		 
				WHERE p.prevention_type = ${dTaPIPVHib}
				AND p.deleted = 0
				AND p.refused = 0 
				GROUP BY p.demographic_no
			) DTaP
			ON (d.demographic_no = DTaP.demographic_no)
		
			 -- WITH  a refusal of any of the following - DTaP-IPV-Hib,  Pneumococcal conjugate,  Meningococcal Conjugate C,  Measles, Mumps, Rubella or Var  - Varicella
                        -- in the past 12 months
                        -- refused in the past 12 months
                        LEFT JOIN (
                                SELECT p.demographic_no, COUNT(p.id) AS id
                                FROM preventions p
                                WHERE p.prevention_type = ${dTaPIPVHib}
                                AND p.deleted = 0
                                AND p.refused = 1
                                AND p.prevention_date > ${lowerLimit.date}
                                GROUP BY p.demographic_no
                        ) DTaP_r
                        ON (d.demographic_no = DTaP_r.demographic_no)

			-- 3 instances of - Pneu-C-13  - Pneumococcal Conjugate 13
			LEFT JOIN ( 
				SELECT p.demographic_no, COUNT(p.id) AS id
				FROM preventions p		 
				WHERE p.prevention_type = ${pneumococcal}
				AND p.deleted = 0
				AND p.refused = 0 
				GROUP BY p.demographic_no
			) PNEU
			ON (d.demographic_no = PNEU.demographic_no)
		
		 	LEFT JOIN (
                              	SELECT p.demographic_no, COUNT(p.id) AS id
                                FROM preventions p
                                WHERE p.prevention_type = ${pneumococcal}
                                AND p.deleted = 0
                                AND p.refused = 1
                                AND p.prevention_date > ${lowerLimit.date}
                                GROUP BY p.demographic_no
                        ) PNEU_r
                        ON (d.demographic_no = PNEU_r.demographic_no)
		
			-- 1 instance of - Men-C-C  - Meningococcal Conjugate 
			LEFT JOIN ( 
				SELECT p.demographic_no, COUNT(p.id) AS id
				FROM preventions p		 
				WHERE p.prevention_type = ${meningococcal}
				AND p.deleted = 0
				AND p.refused = 0
				GROUP BY p.demographic_no 
			) MC
			ON (d.demographic_no = MC.demographic_no)
		
			LEFT JOIN (
                                SELECT p.demographic_no, COUNT(p.id) AS id
                                FROM preventions p
                                WHERE p.prevention_type = ${meningococcal}
                                AND p.deleted = 0
                                AND p.refused = 1
                                AND p.prevention_date > ${lowerLimit.date}
                                GROUP BY p.demographic_no
                        ) MC_r
                        ON (d.demographic_no = MC_r.demographic_no)

			-- 1 instance of - MMR  - Measles, Mumps, Rubella
			LEFT JOIN ( 
				SELECT p.demographic_no, COUNT(p.id) AS id
				FROM preventions p		 
				WHERE p.prevention_type = ${measlesMumpsRubella}
				AND p.deleted = 0
				AND p.refused = 0
				GROUP BY p.demographic_no
			) MM
			ON (d.demographic_no = MM.demographic_no)
		
			LEFT JOIN (
                                SELECT p.demographic_no, COUNT(p.id) AS id
                                FROM preventions p
                                WHERE p.prevention_type = ${measlesMumpsRubella}
                                AND p.deleted = 0
                                AND p.refused = 1
                                AND p.prevention_date > ${lowerLimit.date}
                                GROUP BY p.demographic_no
                        ) MM_r
                        ON (d.demographic_no = MM_r.demographic_no)

			-- 1 instance of - Var  - Varicella
			LEFT JOIN ( 
				SELECT p.demographic_no, COUNT(p.id) AS id
				FROM preventions p		 
				WHERE p.prevention_type = ${varicella}
				AND p.deleted = 0
				AND p.refused = 0
				GROUP BY p.demographic_no
			) VZ
			ON (d.demographic_no = VZ.demographic_no)
			
			 LEFT JOIN (
                                SELECT p.demographic_no, COUNT(p.id) AS id
                                FROM preventions p
                                WHERE p.prevention_type = ${varicella}
                                AND p.deleted = 0
                                AND p.refused = 1
                                AND p.prevention_date > ${lowerLimit.date}
                                GROUP BY p.demographic_no
                        ) VZ_r
                        ON (d.demographic_no = VZ_r.demographic_no)

			-- instances of - Rot-1  - Rotavirus
			LEFT JOIN ( 
				SELECT p.demographic_no, COUNT(p.id) AS id
				FROM preventions p		 
				WHERE p.prevention_type = ${rotavirus}
				AND p.deleted = 0
				AND p.refused = 0
 				GROUP BY p.demographic_no
			) ROT
			ON (d.demographic_no = ROT.demographic_no)
		
			-- WITH  a refusal of any of the following - DTaP-IPV-Hib,  Pneumococcal conjugate,  Meningococcal Conjugate C,  Measles, Mumps, Rubella or Var  - Varicella
			
			LEFT JOIN (
                                SELECT p.demographic_no, 'Yes' AS refused
                                FROM preventions p
                                WHERE p.prevention_type = ${dTaPIPVHib}
                                AND p.deleted = 0
                                AND p.refused = 1
                                GROUP BY p.demographic_no
                        ) REF_dtap
                        ON (d.demographic_no = REF_dtap.demographic_no)
	
			LEFT JOIN (
                                SELECT p.demographic_no, 'Yes' AS refused
                                FROM preventions p
                                WHERE p.prevention_type = ${pneumococcal}
                                AND p.deleted = 0
                                AND p.refused = 1
                                GROUP BY p.demographic_no
                        ) REF_pneu
                        ON (d.demographic_no = REF_pneu.demographic_no)

			LEFT JOIN (
                                SELECT p.demographic_no, 'Yes' AS refused
                                FROM preventions p
                                WHERE p.prevention_type = ${meningococcal}
                                AND p.deleted = 0
                                AND p.refused = 1
                                GROUP BY p.demographic_no
                        ) REF_men
                        ON (d.demographic_no = REF_men.demographic_no)
		
			LEFT JOIN (
                                SELECT p.demographic_no, 'Yes' AS refused
                                FROM preventions p
                                WHERE p.prevention_type = ${varicella}
                                AND p.deleted = 0
                                AND p.refused = 1
                                GROUP BY p.demographic_no
                        ) REF_var
                        ON (d.demographic_no = REF_var.demographic_no)


			LEFT JOIN (
                                SELECT p.demographic_no, 'Yes' AS refused
                                FROM preventions p
                                WHERE p.prevention_type = ${measlesMumpsRubella}
                                AND p.deleted = 0
                                AND p.refused = 1
                                GROUP BY p.demographic_no
                        ) REF_mmr
                        ON (d.demographic_no = REF_mmr.demographic_no)

			 LEFT JOIN (
                                SELECT p.demographic_no, 'Yes' AS refused
                                FROM preventions p
                                WHERE p.prevention_type = ${varicella}
                                AND p.deleted = 0
                                AND p.refused = 1
                                GROUP BY p.demographic_no
                        ) REF_vz
                        ON (d.demographic_no = REF_vz.demographic_no)

			LEFT JOIN (
                                SELECT p.demographic_no, 'Yes' AS refused
                                FROM preventions p
                                WHERE p.prevention_type = ${rotavirus}
                                AND p.deleted = 0
                                AND p.refused = 1
                                GROUP BY p.demographic_no
                        ) REF_rot
                        ON (d.demographic_no = REF_rot.demographic_no)
			
			-- PATIENTS BETWEEN 30 TO 42 MONTHS OLD
			WHERE d.patient_status = ${pstatus}
			AND d.provider_no = '${provider}'
			AND d.demographic_no > 0 
			AND CURRENT_DATE() >= DATE_ADD(DATE( CONCAT(d.year_of_birth,"-",d.month_of_birth,"-",d.date_of_birth) ), INTERVAL ${lowerLimit.age} MONTH) 
			AND DATE_ADD(DATE( CONCAT(d.year_of_birth,"-",d.month_of_birth,"-",d.date_of_birth) ), INTERVAL ${upperLimit.age} MONTH) >= CURRENT_DATE()	

			AND ( ( ((DTaP.id IS NULL OR 4 > DTaP.id) AND ( DTaP_r.id = 0 OR DTaP_r.id IS NULL) ) 
					AND (PNEU.id >= 3 OR PNEU_r.id = 0 OR PNEU_r.id IS NULL )
					AND (MC.id >= 1 OR MC_r.id = 0 OR MC_r.id IS NULL )
					AND (MM.id >= 1 OR MM_r.id = 0 OR MM_r.id IS NULL )
					AND (VZ.id >= 1 OR VZ_r.id = 0 OR VZ_r.id IS NULL ) )
				OR ( ((PNEU.id IS NULL OR 3 > PNEU.id) AND (PNEU_r.id = 0 OR PNEU_r.id IS NULL))
					AND (DTaP.id >= 4 OR DTaP_r.id = 0 OR DTaP_r.id IS NULL )
                                        AND (MC.id >= 1 OR MC_r.id = 0 OR MC_r.id IS NULL )
                                        AND (MM.id >= 1 OR MM_r.id = 0 OR MM_r.id IS NULL )
                                        AND (VZ.id >= 1 OR VZ_r.id = 0 OR VZ_r.id IS NULL ) )
				OR ( ((MC.id IS NULL OR 1 > MC.id) AND (MC_r.id = 0 OR MC_r.id IS NULL))
					AND (DTaP.id >= 4 OR DTaP_r.id = 0 OR DTaP_r.id IS NULL )
                                        AND (PNEU.id >= 3 OR PNEU_r.id = 0 OR PNEU_r.id IS NULL )
                                        AND (MM.id >= 1 OR MM_r.id = 0 OR MM_r.id IS NULL )
                                        AND (VZ.id >= 1 OR VZ_r.id = 0 OR VZ_r.id IS NULL ) )
				OR ( ((MM.id IS NULL OR 1 > MM.id) AND (MM_r.id = 0 OR MM_r.id IS NULL))
					AND (DTaP.id >= 4 OR DTaP_r.id = 0 OR DTaP_r.id IS NULL )
                                        AND (PNEU.id >= 3 OR PNEU_r.id = 0 OR PNEU_r.id IS NULL )
                                        AND (MC.id >= 1 OR MC_r.id = 0 OR MC_r.id IS NULL )
                                        AND (VZ.id >= 1 OR VZ_r.id = 0 OR VZ_r.id IS NULL ) )
				OR ( ((VZ.id IS NULL OR 1 > VZ.id) AND (VZ_r.id = 0 OR VZ_r.id IS NULL))
					AND (DTaP.id >= 4 OR DTaP_r.id = 0 OR DTaP_r.id IS NULL )
                                        AND (PNEU.id >= 3 OR PNEU_r.id = 0 OR PNEU_r.id IS NULL )
                                        AND (MC.id >= 1 OR MC_r.id = 0 OR MC_r.id IS NULL )
                                        AND (MM.id >= 1 OR MM_r.id = 0 OR MM_r.id IS NULL ) )
			)

				
		</query>
	</drillDownQuery>
	<shared>false</shared>
	<sharedMetricSetName>OntarioMD Childhood Immunization Recall</sharedMetricSetName>
	<sharedMetricDataId>Status</sharedMetricDataId>
	<sharedMappings>
		<sharedMapping fromLabel="% Complete" toLabel="Complete"/>
		<sharedMapping fromLabel="% Incomplete" toLabel="Incomplete"/>
		<sharedMapping fromLabel="% Declined one or more" toLabel="Declined one or more"/>
	</sharedMappings>
</indicatorTemplateXML>
