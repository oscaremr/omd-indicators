<?xml version="1.0" encoding="UTF-8"?> 
<indicatorTemplateXML> 
	<author>Colcamex Resources Inc. for Oscar EMR</author> 
	<uid></uid> 
	<heading> 
		<category>CDM</category> 
		<subCategory>OMD-Hypertension</subCategory> 
		<name>OMD-Hypertension</name> 
		<metricSetName>OntarioMD HTN Testing Status</metricSetName>
		<metricLabel></metricLabel>
		<definition>% of patient population, age 18 and older, with hypertension who received testing within the past 12 months, for any of the following: Blood glucose or HbA1C, BP measurement, Obesity/overweight screening.</definition> 
		<framework>Based on and adapted from CIHI’s 2012 Indicator Technical Specifications (Nov 2012)</framework> 
		<frameworkVersion>01-23-2018</frameworkVersion> 
		<notes></notes> 
	</heading> 
	<indicatorQuery> 
		<version>01-23-2018</version> 
		<params> <!-- Required parameter provider. Value options are: [ an individual providerNo, or provider range ] ie: value="370, 1001" "all" ie: value="all" including null.  "loggedInProvider" ie: Default is "loggedInProvider" Use this parameter in the query as ${provider} This parameter should be used for fetching patient's assigned to a MRP.  ie: WHERE demographic.provider_no = ${provider} --> 
			<parameter id="provider" name="provider_no" value="loggedInProvider" /> 
			<parameter id="hba1c" name="HbA1C LOINC" value="17856-6, 4548-4, 17855-8, 71875-9 " /> 
			<parameter id="a1c" name="A1C" value="HbA1C, A1C" /> 
			<parameter id="bloodGlucose" name="Blood Glucose" value="'FBS'" /> 
			<parameter id="bloodPressure" name="Blood Pressure" value="'BP'" /> 
			<parameter id="bmi" name="Body Mass Index" value="'BMI'" /> 
			<parameter id="height" name="Height" value="'HT'" />
			<parameter id="weight" name="Weight" value="'WT'" />
			<parameter id="weight_circ" name="Waist Circumference" value="'WAIS'" />
			<parameter id="pstatus" name="Patient Status" value="'AC'" /> 
			<parameter id="dxcodes" name="Dx Research Codes" value="401,402,403,404,405,I10,D3-02120,D3-02010,D3-02100,D3-02000,1201005,10725009,59621000,38341003" />		
		</params> 
		<range> 
			<upperLimit id="date" label="Date Today" name="Date" value="NOW()" />
			<lowerLimit id="date" label="12 Months History" name="Date" value="DATE_SUB( NOW(), INTERVAL 12 MONTH )" />
			
			<lowerLimit id="age" label="Minimum Age" name="Age" value="18" />
		</range>
		<query>
			<!-- Indicator SQL Query here -->
			SELECT 

				IF ( COUNT(fin.patient) > 0,
                                SUM( IF( fin.bp > 0 OR fin.bg > 0 OR fin.a1c > 0 OR fin.a1c_m > 0 OR fin.bmi > 0 OR (fin.ht > 0 AND fin.wt > 0) OR (fin.ht > 0 AND fin.wc > 0), 1, 0 ) )
                                ,0 ) AS '% Patients with LIPID, BP or BMI',

                                IF ( COUNT(fin.patient) > 0,
                                SUM( IF( fin.bp IS NULL AND fin.bg IS NULL AND fin.a1c IS NULL AND fin.a1c_m IS NULL AND fin.bmi IS NULL AND ( fin.ht IS NULL OR ( fin.ht IS NOT NULL AND ( fin.wt IS NULL AND fin.wc IS NULL ) ) ) , 1, 0 ) )
                                ,0 ) AS '% Patients with no test'

			FROM (
			
				SELECT 
					d.demographic_no AS patient,
					BP.id AS bp,
					BG.id AS bg,
					BMI.id AS bmi,
					HT.id AS ht,
					WT.id AS wt,
					WC.id AS wc,
					A1C.id AS a1c,
					A1C_m.id AS a1c_m
				FROM demographic d 
				INNER JOIN dxresearch dxr 
				ON ( d.demographic_no = dxr.demographic_no) 
			
				-- HbA1C 
			        LEFT JOIN (
                                	SELECT m.demographicNo, m.id, m.dataField
                                	FROM measurements m
                                	JOIN (
                                        	SELECT me.measurement_id AS id
                                        	FROM measurementsExt me
                                        	JOIN measurementMap mm
                                        	ON me.val = mm.`name`
                                        	WHERE me.keyval = "name"
                                        	AND mm.loinc_code IN ${hba1c}
                                        	GROUP BY me.measurement_id
                                	) m1
                                	ON m.id = m1.id 
                                	WHERE  DATE(m.dateObserved) > ${lowerLimit.date}
                                	GROUP BY m.demographicNo HAVING COUNT(m.demographicNo) > -1
                        	) A1C
                        	ON (d.demographic_no = A1C.demographicNo)

				-- A1C in measurements
				LEFT JOIN (
                                        SELECT id, dataField, demographicNo
                                        FROM measurements
                                        WHERE type IN ${a1c}
                                        AND DATE(dateObserved) > ${lowerLimit.date}
                                        AND demographicNo > 0
                                        GROUP BY demographicNo HAVING COUNT(demographicNo) > -1
                                ) A1C_m
                                ON (d.demographic_no = A1C_m.demographicNo)
		

				-- or Blood Glucose
				LEFT JOIN (
					SELECT id, dataField, demographicNo 
					FROM measurements 
					WHERE type = ${bloodGlucose}
					AND DATE(dateObserved) > ${lowerLimit.date} 
					AND demographicNo > 0 
					GROUP BY demographicNo HAVING COUNT(demographicNo) > -1
				) BG
				ON (d.demographic_no = BG.demographicNo)
			
			
				-- Blood Pressure
				LEFT JOIN (
					SELECT id, dataField, demographicNo 
					FROM measurements 
					WHERE type = ${bloodPressure}
					AND DATE(dateObserved) > ${lowerLimit.date} 
					AND demographicNo > 0 
					GROUP BY demographicNo HAVING COUNT(demographicNo) > -1
				) BP
				ON (d.demographic_no = BP.demographicNo)
			
				-- BMI Screening
				-- Look for BMI index or if the height and weight was measured.
				LEFT JOIN ( 
					SELECT id, dataField, demographicNo 
					FROM measurements 
					WHERE type = ${bmi} 
					AND DATE(dateObserved) > ${lowerLimit.date} 
					AND demographicNo > 0 
					GROUP BY demographicNo HAVING COUNT(demographicNo) > -1
				) BMI 
				ON (d.demographic_no = BMI.demographicNo)

				--HT 
				LEFT JOIN (
                                        SELECT id, dataField, demographicNo
                                        FROM measurements
                                        WHERE type = ${height}
                                        AND DATE(dateObserved) > ${lowerLimit.date}
                                        AND demographicNo > 0
                                        GROUP BY demographicNo HAVING COUNT(demographicNo) > -1
                                ) HT
                                ON (d.demographic_no = HT.demographicNo)

				-- WT
			 	LEFT JOIN (
                                        SELECT id, dataField, demographicNo
                                        FROM measurements
                                        WHERE type = ${weight}
                                        AND DATE(dateObserved) > ${lowerLimit.date}
                                        AND demographicNo > 0
                                        GROUP BY demographicNo HAVING COUNT(demographicNo) > -1
                                ) WT
                                ON (d.demographic_no = WT.demographicNo)

				-- Waist Circumference
				LEFT JOIN (
                                        SELECT id, dataField, demographicNo
                                        FROM measurements
                                        WHERE type = ${weight_circ}
                                        AND DATE(dateObserved) > ${lowerLimit.date}
                                        AND demographicNo > 0
                                        GROUP BY demographicNo HAVING COUNT(demographicNo) > -1
                                ) WC
                                ON (d.demographic_no = WC.demographicNo)

				WHERE d.patient_status = ${pstatus} 
				AND provider_no = '${provider}'
				AND d.demographic_no > 0 
				AND dxr.dxresearch_code IN ${dxcodes}
				AND dxr.`status` != "D" 
				AND ( ROUND( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,"-",d.month_of_birth,"-",d.date_of_birth) ), 
					NOW() ) ) / 365.25 ) >=  ${lowerLimit.age} )
				GROUP BY d.demographic_no HAVING COUNT(d.demographic_no) > -1
			) fin
		</query>
	</indicatorQuery>
	<drillDownQuery>
		<version>01-23-2018</version>
		<params>
			<parameter id="provider" name="provider_no" value="loggedInProvider" />
			<parameter id="hba1c" name="HbA1C LOINC" value="17856-6, 4548-4, 17855-8, 71875-9" />
			<parameter id="a1c" name="A1C" value="HbA1C, A1C" /> 
			<parameter id="bloodGlucose" name="Blood Glucose" value="'FBS'" />
			<parameter id="bloodPressure" name="Blood Pressure" value="'BP'" />
			<parameter id="bmi" name="Body Mass Index" value="'BMI'" />
			<parameter id="height" name="Height" value="'HT'" />
			<parameter id="weight" name="Weight" value="'WT'" />
			<parameter id="weight_circ" name="Waist Circumference" value="'WAIS'" />
			<parameter id="pstatus" name="Patient Status" value="'AC'" />	
			<parameter id="dxcodes" name="Dx Research Codes" value="401,402,403,404,405,I10,D3-02120,D3-02010,D3-02100,D3-02000,1201005,10725009,59621000,38341003" />					
                        <parameter id="sharedMetricLabel" name="sharedMetricLabel" value="sharedMetricLabel" />

		</params>
		<range>
			<upperLimit id="date" label="Date Today" name="Date" value="NOW()" />
			<lowerLimit id="date" label="12 Months History" name="Date" value="DATE_SUB( NOW(), INTERVAL 12 MONTH )" />
			
			<lowerLimit id="age" label="Minimum Age" name="Age" value="18" />
		</range>
		<displayColumns>
			<column id="demographic" name="d.demographic_no" title="Patient Id" primary="true" />
			<column id="name" name="CONCAT( d.last_name, ', ', d.first_name )" title="Patient Name" primary="false" />
			<column id="dob" name="DATE_FORMAT( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth), '%m-%d-%Y' )" title="Patient DOB (mm-dd-yyyy)" primary="false" />
			<column id="age" name="FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth) ), NOW() ) ) / 365.25 )" title="Patient Age" primary="false" />
			<column id="phonenumber" name="IFNULL( d.phone, '')" title="Patient Phone Number" primary="false" />
			<column id="hba1c" name="IFNULL( A1C_m0.dataField, '')" title="Most Recent HbA1C" primary="false" />
			<column id="hba1cdate" title="Date of Most Recent HbA1c (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( A1C_m0.dateObserved, '%m-%d-%Y' ), '')" primary="false" />
			<column id="bg" title="Most Recent Blood Glucose" name="IFNULL( BG0.dataField, '')" primary="false"/>
			<column id="bgdate" title="Date of Most Recent Blood Glucose (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( BG0.dateObserved, '%m-%d-%Y' ), '')" primary="false" />
			<column id="bp" title="Most Recent BP" name="IFNULL( BP0.dataField, '')" primary="false" />
			<column id="bpdate" title="Date of Most Recent BP (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( BP0.dateObserved, '%m-%d-%Y' ), '')" primary="false" />
			<column id="bmi" title="Most Recent BMI" name="IFNULL( BMI0.dataField, '')" primary="false" />
			<column id="bmidate" title="Date of Most Recent BMI (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( BMI0.dateObserved, '%m-%d-%Y' ), '')" primary="false" />
			<column id="ht" title="Most Recent Height" name="IFNULL( HT0.dataField, '')" primary="false" />
			<column id="htdate" title="Date of Most Recent Height (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( HT0.dateObserved, '%m-%d-%Y' ), '')" primary="false" />
			<column id="wt" title="Most Recent Weight" name="IFNULL( WT0.dataField, '')" primary="false" />
			<column id="wtdate" title="Date of Most Recent Weight (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( WT0.dateObserved, '%m-%d-%Y' ), '')" primary="false" />
			<column id="wc" title="Most Recent Waist Circumference" name="IFNULL( WC0.dataField, '')" primary="false" />
			<column id="wcdate" title="Date of Most Recent Waist Circumference (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( WC0.dateObserved, '%m-%d-%Y' ), '')" primary="false" />
<!--
			<column id="dxcode" title="Dx System/Code" name="CONCAT(MIN(dxr.coding_system),'/',MIN(dxr.dxresearch_code),IF( MIN(dxr.dxresearch_code) != MAX(dxr.dxresearch_code),CONCAT( ' &amp; ', MAX(dxr.coding_system), '/', MAX(dxr.dxresearch_code) ),'') )" primary="false" />
-->
			<column id="lastappdate" name="IFNULL( DATE_FORMAT( app1.appointment_date, '%m-%d-%Y' ), '')" title="Last Seen Date (mm-dd-yyyy)" primary="false" />
			<column id="nextappdate" name="IFNULL( DATE_FORMAT( app2.appointment_date, '%m-%d-%Y' ), '')" title="Next Appointment Date (mm-dd-yyyy)" primary="false" />
		</displayColumns>
		<exportColumns>
			<column id="demographic" name="d.demographic_no" title="Patient Id" primary="true" />
			<column id="firstName" name="d.first_name" title="First Name" primary="false" />
			<column id="lastName" name="d.last_name" title="Last Name" primary="false" />
			<column id="dob" name="DATE_FORMAT( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth), '%m-%d-%Y' )" title="Patient DOB (mm-dd-yyyy)" primary="false" />
                        <column id="age" name="FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth) ), NOW() ) ) / 365.25 )" title="Patient Age" primary="false" />
                        <column id="phonenumber" name="IFNULL( d.phone, '')" title="Patient Phone Number" primary="false" />
                        <column id="hba1c" name="IFNULL( A1C_m0.dataField, '')" title="Most Recent HbA1C" primary="false" />
                        <column id="hba1cdate" title="Date of Most Recent HbA1c (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( A1C_m0.dateObserved, '%m-%d-%Y' ), '')" primary="false" />
                        <column id="bg" title="Most Recent Blood Glucose" name="IFNULL( BG0.dataField, '')" primary="false"/>
                        <column id="bgdate" title="Date of Most Recent Blood Glucose (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( BG0.dateObserved, '%m-%d-%Y' ), '')" primary="false" />
                        <column id="bp" title="Most Recent BP" name="IFNULL( BP0.dataField, '')" primary="false" />
                        <column id="bpdate" title="Date of Most Recent BP (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( BP0.dateObserved, '%m-%d-%Y' ), '')" primary="false" />
                        <column id="bmi" title="Most Recent BMI" name="IFNULL( BMI0.dataField, '')" primary="false" />
                        <column id="bmidate" title="Date of Most Recent BMI (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( BMI0.dateObserved, '%m-%d-%Y' ), '')" primary="false" />
                        <column id="ht" title="Most Recent Height" name="IFNULL( HT0.dataField, '')" primary="false" />
                        <column id="htdate" title="Date of Most Recent Height (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( HT0.dateObserved, '%m-%d-%Y' ), '')" primary="false" />
                        <column id="wt" title="Most Recent Weight" name="IFNULL( WT0.dataField, '')" primary="false" />
                        <column id="wtdate" title="Date of Most Recent Weight (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( WT0.dateObserved, '%m-%d-%Y' ), '')" primary="false" />
                        <column id="wc" title="Most Recent Waist Circumference" name="IFNULL( WC0.dataField, '')" primary="false" />
                        <column id="wcdate" title="Date of Most Recent Weight Circumference (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( WC0.dateObserved, '%m-%d-%Y' ), '')" primary="false" />

<!--
			<column id="dxcode" title="Dx System/Code" name="CONCAT(MIN(dxr.coding_system),'/',MIN(dxr.dxresearch_code),IF( MIN(dxr.dxresearch_code) != MAX(dxr.dxresearch_code),CONCAT( ' &amp; ', MAX(dxr.coding_system), '/', MAX(dxr.dxresearch_code) ),'') )" primary="false" />
-->
			<column id="lastappdate" name="IFNULL( DATE_FORMAT( app1.appointment_date, '%m-%d-%Y' ), '')" title="Last Seen Date (mm-dd-yyyy)" primary="false" />
			<column id="nextappdate" name="IFNULL( DATE_FORMAT( app2.appointment_date, '%m-%d-%Y' ), '')" title="Next Appointment Date (mm-dd-yyyy)" primary="false" />
		</exportColumns>
		<query>
			<!-- Drilldown SQL Query here -->	
			SELECT 
			*
			FROM demographic d 
			INNER JOIN dxresearch dxr 
			ON ( d.demographic_no = dxr.demographic_no) 
		
			LEFT JOIN ( 
			        SELECT m.demographicNo, m.id, m.dataField
                                FROM measurements m
                                JOIN (
                                        SELECT me.measurement_id AS id
                                        FROM measurementsExt me
                                        JOIN measurementMap mm
                                        ON me.val = mm.`name`
                                        WHERE me.keyval = "name"
                                        AND mm.loinc_code IN ${hba1c}
                                        GROUP BY me.measurement_id
                                ) m1
                                ON m.id = m1.id 
                                WHERE  DATE(m.dateObserved) > ${lowerLimit.date}
                                GROUP BY m.demographicNo HAVING COUNT(m.demographicNo) > -1
                        ) A1C
                        ON (d.demographic_no = A1C.demographicNo)
				
			-- all HbA1C
--			LEFT JOIN (
 --                               SELECT m.demographicNo, m.id, m.dataField, MAX(m.dateObserved) AS dateObserved
  --                              FROM measurements m
   --                             JOIN (
    --                                    SELECT m2.demographicNo, MAX(m2.id) AS id
     --                                   FROM measurements m2 
--					JOIN measurementsExt me
--					ON m2.id = me.measurement_id
         --                             JOIN measurementMap mm
 --                                       ON me.val = mm.`name`
  --                                      WHERE me.keyval = "name"
   --                                     AND mm.loinc_code IN ${hba1c}
    --                                    GROUP BY m2.demographicNo
     --                           ) m1
          --                      ON m.id = m1.id
      --                          GROUP BY m.demographicNo HAVING COUNT(m.demographicNo) > -1
       --                 ) A1C0
        ----                ON (d.demographic_no = A1C0.demographicNo)

			 -- A1C in measurements
                        LEFT JOIN (
                                SELECT id, dataField, demographicNo
                                FROM measurements
                                WHERE type IN ${a1c}
                                AND DATE(dateObserved) > ${lowerLimit.date}
                                AND demographicNo > 0
                                GROUP BY demographicNo HAVING COUNT(demographicNo) > -1
                        ) A1C_m
                        ON (d.demographic_no = A1C_m.demographicNo)
				
			LEFT JOIN (
                                SELECT m1.id, m1.dataField, m1.demographicNo, m1.dateObserved
                                FROM measurements m1
                                RIGHT JOIN (
					SELECT demographicNo, MAX(DateObserved) as dateObserved
                                        FROM measurements
                                        WHERE type IN ${a1c}
                                        AND demographicNo > 0
                                        GROUP BY demographicNo
                                ) m2
				ON m1.dateObserved = m2.dateObserved
                                AND m1.demographicNo = m2.demographicNo
				AND m1.type IN ${a1c}
                                ORDER BY m1.id DESC

                        ) A1C_m0
                        ON (d.demographic_no = A1C_m0.demographicNo)

			-- or Blood Glucose
                        LEFT JOIN (
                                SELECT id, dataField, demographicNo
                                FROM measurements
                                WHERE type = ${bloodGlucose}
                                AND DATE(dateObserved) > ${lowerLimit.date}
                                AND demographicNo > 0
                                GROUP BY demographicNo HAVING COUNT(demographicNo) > -1
                        ) BG
                        ON (d.demographic_no = BG.demographicNo)

			-- all BG
		 	LEFT JOIN (
                                SELECT m1.id, m1.dataField, m1.demographicNo, m1.dateObserved
                                FROM measurements m1
				RIGHT JOIN (
					SELECT demographicNo, MAX(DateObserved) as dateObserved
                                        FROM measurements
                                        WHERE type = ${bloodGlucose}
                                        AND demographicNo > 0
                                        GROUP BY demographicNo
                                ) m2
				ON m1.dateObserved = m2.dateObserved
                                AND m1.demographicNo = m2.demographicNo
				AND m1.type = ${bloodGlucose}
                                ORDER BY m1.id DESC

                        ) BG0
                        ON (d.demographic_no = BG0.demographicNo)

                        -- Blood Pressure
                        LEFT JOIN (
                                SELECT id, dataField, demographicNo
                                FROM measurements
                                WHERE type = ${bloodPressure}
                                AND DATE(dateObserved) > ${lowerLimit.date}
                                AND demographicNo > 0
                                GROUP BY demographicNo HAVING COUNT(demographicNo) > -1
                        ) BP
                        ON (d.demographic_no = BP.demographicNo)

			-- all BP
			
			LEFT JOIN (
                                SELECT m1.id, m1.dataField, m1.demographicNo, m1.dateObserved
                                FROM measurements m1
                                RIGHT JOIN (
                                        SELECT demographicNo, MAX(DateObserved) as dateObserved
                                        FROM measurements
                                        WHERE type = ${bloodPressure}
                                        AND demographicNo > 0
                                        GROUP BY demographicNo
                                ) m2
                                ON m1.dateObserved = m2.dateObserved
				AND m1.demographicNo = m2.demographicNo
				AND m1.type = ${bloodPressure}
				ORDER BY m1.id DESC
                        ) BP0
                        ON (d.demographic_no = BP0.demographicNo)

                        -- BMI Screening
                        -- Look for BMI index or if the height and weight was measured.
                        LEFT JOIN (
                                SELECT id, dataField, demographicNo
                                FROM measurements
                                WHERE type = ${bmi}
                                AND DATE(dateObserved) > ${lowerLimit.date}
                                AND demographicNo > 0
                                GROUP BY demographicNo HAVING COUNT(demographicNo) > -1
                        ) BMI
                        ON (d.demographic_no = BMI.demographicNo)

			-- all BMI
		  	LEFT JOIN (
                                SELECT m1.id, m1.dataField, m1.demographicNo, m1.dateObserved
                                FROM measurements m1
				RIGHT JOIN (
					SELECT demographicNo, MAX(DateObserved) as dateObserved
                                        FROM measurements
                                        WHERE type = ${bmi}
                                        AND demographicNo > 0
                                        GROUP BY demographicNo
                                ) m2
				ON m1.dateObserved = m2.dateObserved
                                AND m1.demographicNo = m2.demographicNo
				AND m1.type = ${bmi}
                                ORDER BY m1.id DESC

                        ) BMI0
                        ON (d.demographic_no = BMI0.demographicNo)
			

                        -- height 
		 	LEFT JOIN (
                                SELECT id, dataField, demographicNo
                                FROM measurements
                                WHERE type = ${height}
                                AND DATE(dateObserved) > ${lowerLimit.date}
                                AND demographicNo > 0
                                GROUP BY demographicNo HAVING COUNT(demographicNo) > -1
                        ) HT
                        ON (d.demographic_no = HT.demographicNo) 

			-- all height
			LEFT JOIN (
                                SELECT m1.id, m1.dataField, m1.demographicNo, m1.dateObserved
                                FROM measurements m1
				RIGHT JOIN (
					SELECT demographicNo, MAX(DateObserved) as dateObserved
                                        FROM measurements
                                        WHERE type = ${height}
                                        AND demographicNo > 0
                                        GROUP BY demographicNo
                                ) m2
				ON m1.dateObserved = m2.dateObserved
                                AND m1.demographicNo = m2.demographicNo
				AND m1.type = ${height}
                                ORDER BY m1.id DESC
                        ) HT0
                        ON (d.demographic_no = HT0.demographicNo)


			-- weight
                        LEFT JOIN (
                                SELECT id, dataField, demographicNo
                                FROM measurements
                                WHERE type = ${weight}
                                AND DATE(dateObserved) > ${lowerLimit.date}
                                AND demographicNo > 0
                                GROUP BY demographicNo HAVING COUNT(demographicNo) > -1
                        ) WT
                        ON (d.demographic_no = WT.demographicNo)  

                        -- all weight
                        LEFT JOIN (
                                SELECT m1.id, m1.dataField, m1.demographicNo, m1.dateObserved
                                FROM measurements m1
				RIGHT JOIN (
					SELECT demographicNo, MAX(DateObserved) as dateObserved
                                        FROM measurements
                                        WHERE type = ${weight}
                                        AND demographicNo > 0
                                        GROUP BY demographicNo
                                ) m2
				ON m1.dateObserved = m2.dateObserved
                                AND m1.demographicNo = m2.demographicNo
				AND m1.type = ${weight}
                                ORDER BY m1.id DESC

                        ) WT0
                        ON (d.demographic_no = WT0.demographicNo)

			-- weight circumference
                        LEFT JOIN (
                                SELECT id, dataField, demographicNo
                                FROM measurements
                                WHERE type = ${weight_circ}
                                AND DATE(dateObserved) > ${lowerLimit.date}
                                AND demographicNo > 0
                                GROUP BY demographicNo HAVING COUNT(demographicNo) > -1
                        ) WC
                        ON (d.demographic_no = WC.demographicNo)

                        -- all weight circumference
                        LEFT JOIN (
                                SELECT m1.id, m1.dataField, m1.demographicNo, m1.dateObserved
                                FROM measurements m1
				RIGHT JOIN (
					SELECT demographicNo, MAX(DateObserved) as dateObserved
                                        FROM measurements
                                        WHERE type = ${weight_circ}
                                        AND demographicNo > 0
                                        GROUP BY demographicNo
                                ) m2
				ON m1.dateObserved = m2.dateObserved
                                AND m1.demographicNo = m2.demographicNo
				AND m1.type = ${weight_circ}
                                ORDER BY m1.id DESC

                        ) WC0
                        ON (d.demographic_no = WC0.demographicNo)

			-- Date of most recent appointment
			LEFT JOIN (
        			SELECT a.demographic_no, max(a.appointment_date) as appointment_date
        			FROM appointment a, demographic d
        			WHERE a.demographic_no = d.demographic_no
        			AND d.provider_no = '${provider}'
        			AND ${upperLimit.date} > DATE(a.appointment_date)
        			AND a.status != 'D' AND a.status != 'C' AND a.status != 'N'
        			AND a.demographic_no > 0
        			GROUP BY a.demographic_no
			) app1
			ON (d.demographic_no = app1.demographic_no)


 			-- Next Appointment Date
			LEFT JOIN (
        			SELECT a.demographic_no, a.appointment_date
        			FROM appointment a, demographic d
        			WHERE a.demographic_no = d.demographic_no
        			AND d.provider_no = '${provider}'
        			AND DATE(a.appointment_date) >= ${upperLimit.date}
        			AND a.status != 'D' AND a.status != 'C' AND a.status != 'N'
        			AND a.demographic_no > 0
        			GROUP BY a.demographic_no HAVING COUNT(a.demographic_no) > -1
			) app2
			ON (d.demographic_no = app2.demographic_no)

			WHERE d.patient_status = ${pstatus} 
			AND d.provider_no = '${provider}'
			AND d.demographic_no > 0 
			AND dxr.dxresearch_code IN ${dxcodes}
			AND dxr.`status` != "D" 
			AND ( ROUND( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,"-",d.month_of_birth,"-",d.date_of_birth) ), 
				NOW() ) ) / 365.25 ) >=  ${lowerLimit.age} )
<!--
			AND IF( "${sharedMetricLabel}" = "1+ test up to date", A1C.dataField>0 OR BG.dataField>0 OR BP.dataField>0 OR BMI.dataField>0,  
			    IF( "${sharedMetricLabel}" = "All tests overdue", A1C.dataField IS NULL AND BG.dataField IS NULL AND BP.dataField IS NULL AND BMI.dataField IS NULL, 1=1 ) )
-->
			GROUP BY d.demographic_no HAVING COUNT(d.demographic_no) > -1

		</query>
	</drillDownQuery>
	<shared>true</shared>
	<sharedMetricSetName>OntarioMD HTN Testing Status</sharedMetricSetName>
	<sharedMetricDataId>Status</sharedMetricDataId>
	<sharedMappings>
		<sharedMapping fromLabel="% Patients with LIPID, BP or BMI" toLabel="1+ test up to date"/>
		<sharedMapping fromLabel="% Patients with no test" toLabel="All tests overdue"/>
	</sharedMappings>
</indicatorTemplateXML>
