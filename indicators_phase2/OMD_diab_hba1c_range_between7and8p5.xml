<?xml version="1.0" encoding="UTF-8"?>
<indicatorTemplateXML>
	<author>Colcamex Resources Inc.</author>
	<uid></uid>
	<heading>
		<category>CDM</category>
		<subCategory>OMD-Diabetes</subCategory>
		<name>OMD-Diabetes with HbA1C in Range</name>
		<metricSetName>OntarioMD DM A1C in Range</metricSetName>
		<metricLabel>7% &lt; HbA1C &lt;= 8.5%</metricLabel>
		<definition>
			% of patients with diabetes whose most recent glycemic control in the last 12 months was in the following ranges: 
			HbA1C &lt;= 7%
			7% &lt; HbA1C &lt;= 8.5%
			HbA1C &gt; 8.5%
		</definition>
		<framework>Based on and adapted from AFHTO D2D 3.0 Indicators: Data Dictionary version 4 (Nov 2015)</framework>
		<frameworkVersion>02-05-2018</frameworkVersion>
		<notes>Diabetes HbA1c in range query created for Ontario Medical Doctors</notes>
	</heading>
	<indicatorQuery>
		<version>02-05-2018</version>
		<params>
			<parameter id="loinc" name="LOINC Code" value="17856-6,4548-4,17855-8,71875-9" />
			<parameter id="provider" name="Provider Number" value="loggedInProvider" />
			<parameter id="active" name="Active Patients" value="'AC'" />
			<parameter id="dxcodes" name="Dx Codes" value="250,E10,E11,DB-610,46635009,44054006,73211009" />
			<parameter id="a1c" name="A1C" value="'A1C'" />
		</params>
		<range>
			<lowerLimit id="date" label="From Date" name="Date" value="DATE_SUB( NOW(), INTERVAL 12 MONTH )" />
			<upperLimit id="date" label="Date Today" name="Date" value="NOW()" />
			
			<upperLimit id="a1c" label="Max HbA1C Result" name="A1c Tests" value="8.5" />
			<lowerLimit id="a1c" label="Min HbA1C Result" name="A1c Tests" value="7.0" />
		</range>
		<query>
			SELECT

				IF ( COUNT(fin.patient) &gt; 0,
				SUM( CASE WHEN fin.hba1c &lt;= ${lowerLimit.a1c} OR fin.hba1c_m &lt;= ${lowerLimit.a1c} THEN 1 ELSE 0 END ) 
				, 0) AS "HbA1C &lt;= 7%",
				
				IF ( COUNT(fin.patient) &gt; 0,
				SUM( CASE WHEN ( fin.hba1c &gt; ${lowerLimit.a1c} AND fin.hba1c &lt;= ${upperLimit.a1c} ) OR ( fin.hba1c_m &gt; ${lowerLimit.a1c} AND fin.hba1c_m &lt;= ${upperLimit.a1c} )  THEN 1 ELSE 0 END ) 
				, 0) AS "7% &lt; HbA1C &lt;= 8.5%",
				
				IF ( COUNT(fin.patient) &gt; 0,
				SUM( IF(IFNULL(fin.hba1c,0) = 0 AND IFNULL(fin.hba1c_m,0) = 0 , 1, 0) )
				, 0) AS "HbA1C not done > 12 months",
				
				IF ( COUNT(fin.patient) &gt; 0,
				SUM( CASE WHEN fin.hba1c &gt; ${upperLimit.a1c} OR fin.hba1c_m &gt; ${upperLimit.a1c} THEN 1 ELSE 0 END )
				, 0) AS "HbA1C > 8.5%"
				
			FROM ( 
				SELECT 
					d.demographic_no AS patient, 
					A1C.dataField AS hba1c,
					A1C_m.dataField AS hba1c_m

				FROM demographic d 
				INNER JOIN (
					SELECT demographic_no, MAX(start_date) AS start_date
                                        FROM dxresearch
                                        WHERE `status` != "D"
                                        AND dxresearch_code IN ${dxcodes}
                                        GROUP BY demographic_no
                                ) dxr
                                ON ( d.demographic_no = dxr.demographic_no)

				-- HbA1C measurement
				LEFT JOIN (
                                	SELECT m4.demographicNo, m4.dateObserved, m4.dataField
                                	FROM measurements m4
                                	RIGHT JOIN (
                                        	SELECT m1.demographicNo, m1.dateObserved, max(m1.id) AS id
                                        	FROM measurements m1
                                        	RIGHT JOIN (
                                               		SELECT demographicNo, MAX(dateObserved) as dateObserved
                                                	FROM measurements
                                                	WHERE type = ${a1c}
                                                	AND demographicNo > 0
                                                	AND dateObserved > ${lowerLimit.date}

                                                	GROUP BY demographicNo
                                        	) m2
                                        	ON m1.dateObserved = m2.dateObserved
                                        	AND m1.demographicNo = m2.demographicNo
                                        	AND m1.type = ${a1c}
                                        	GROUP BY m1.demographicNo, m1.dateObserved
                                	) m3
                                	ON m4.id = m3.id

                        	) A1C_m
                        	ON (d.demographic_no = A1C_m.demographicNo)

				LEFT JOIN (
					SELECT m4.demographicNo, m4.dateObserved, m4.dataField
					FROM measurements m4
					RIGHT JOIN (
                                	  	SELECT m.demographicNo, m.dateObserved, max(m.id) AS id
                                	  	FROM measurements m
                                	  	RIGHT JOIN (
                                        		SELECT m2.demographicNo, m2.type, MAX(m2.dateObserved) AS dateObserved
                                        		FROM measurements m2
                                        		JOIN measurementsExt me
                                        		ON m2.id = me.measurement_id
                                        		JOIN measurementMap mm
                                        		ON me.val = mm.`name`
                                        		WHERE me.keyval = "name"
                                        		AND mm.loinc_code IN ${loinc}
							AND m2.dateObserved > ${lowerLimit.date}
                                        		GROUP BY m2.demographicNo, m2.type
                                		) m1
                                		ON m.type = m1.type
                                		AND m.dateObserved = m1.dateObserved
                                		AND m.demographicNo = m1.demographicNo
						GROUP BY m.demographicNo, m.type, m.dateObserved
					) m3
					ON m4.id = m3.id

				) A1C 
				ON (d.demographic_no = A1C.demographicNo) 

				
				WHERE d.patient_status = ${active} 
				AND d.provider_no = '${provider}'
				AND d.demographic_no > 0
			) fin;
		</query>
	</indicatorQuery>
	<drillDownQuery>
		<version>02-05-2018</version>
		<params>
			<parameter id="loinc" name="LOINC Code" value="17856-6,4548-4,17855-8,71875-9" />
			<parameter id="provider" name="Provider Number" value="loggedInProvider" />
			<parameter id="active" name="Active Patients" value="'AC'" />
			<parameter id="dxcodes" name="Dx Codes" value="250,E10,E11,DB-610,46635009,44054006,73211009" />
			<parameter id="a1c" name="A1C" value="'A1C'" />
		</params>
		
		<range>
			<lowerLimit id="date" label="From Date" name="Date" value="DATE_SUB( NOW(), INTERVAL 12 MONTH )" />
			<upperLimit id="date" label="Date Today" name="Date" value="NOW()" />

			<upperLimit id="a1c" label="Max HbA1C Result" name="A1c Tests" value="8.5" />
                        <lowerLimit id="a1c" label="Min HbA1C Result" name="A1c Tests" value="7.0" />

		</range>
		
		<displayColumns>
			<column id="demographic" name="d.demographic_no" title="Patient Id" primary="true" />
			<column id="name" name="CONCAT( d.last_name, ', ', d.first_name )" title="Patient Name" primary="false" />
			<column id="dob" name="DATE_FORMAT( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth), '%m-%d-%Y' )" title="Patient DOB (mm-dd-yyyy)" primary="false" />
			<column id="age" name="FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth) ), NOW() ) ) / 365.25 )" title="Patient Age" primary="false" />
			<column id="phonenumber" name="IFNULL( d.phone, '')" title="Patient Phone Number" primary="false" />
			<column id="a1c" name="IFNULL( A1C_m0.dataField, '')" title="Latest HbA1C Test Value" primary="false" />
			<column id="a1cDate" name="IFNULL( DATE_FORMAT( A1C_m0.dateObserved, '%m-%d-%Y' ), '')" title="Date of Latest HbA1c Test Value (mm-dd-yyyy)" primary="false" />
	<!--		<column id="codeSystem" name="CONCAT( dxr.coding_system, ' ', dxr.dxresearch_code )" title="Dx System/Code" primary="false" />
	-->		<column id="lastappdate" name="IFNULL(  DATE_FORMAT(app1.appointment_date, '%m-%d-%Y' ), '')" title="Last Seen Date (mm-dd-yyyy)" primary="false" />
			<column id="nextappdate" name="IFNULL(  DATE_FORMAT(app2.appointment_date, '%m-%d-%Y' ), '')" title="Next Appointment Date (mm-dd-yyyy)" primary="false" />
		</displayColumns>
		
		<exportColumns>
			<column id="demographic" name="d.demographic_no" title="Patient Id" primary="true" />
			<column id="name" name="CONCAT( d.last_name, ', ', d.first_name )" title="Patient Name" primary="false" />
			<column id="dob" name="DATE_FORMAT( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth), '%m-%d-%Y' )" title="Patient DOB (mm-dd-yyyy)" primary="false" />
                        <column id="age" name="FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth) ), NOW() ) ) / 365.25 )" title="Patient Age" primary="false" />
                        <column id="phonenumber" name="IFNULL( d.phone, '')" title="Patient Phone Number" primary="false" />
                        <column id="a1c" name="IFNULL( A1C_m0.dataField, '')" title="Latest HbA1C Test Value" primary="false" />
                        <column id="a1cDate" name="IFNULL( DATE_FORMAT( A1C_m0.dateObserved, '%m-%d-%Y' ), '')" title="Date of Latest HbA1c Test Value (mm-dd-yyyy)" primary="false" />
        <!--            <column id="codeSystem" name="CONCAT( dxr.coding_system, ' ', dxr.dxresearch_code )" title="Dx System/Code" primary="false" />
        -->             <column id="lastappdate" name="IFNULL(  DATE_FORMAT(app1.appointment_date, '%m-%d-%Y' ), '')" title="Last Seen Date (mm-dd-yyyy)" primary="false" />
                        <column id="nextappdate" name="IFNULL(  DATE_FORMAT(app2.appointment_date, '%m-%d-%Y' ), '')" title="Next Appointment Date (mm-dd-yyyy)" primary="false" />

		</exportColumns>
		<query>	
			SELECT *	
			FROM demographic d 
			INNER JOIN (
				SELECT demographic_no, MAX(start_date) AS start_date
                                FROM dxresearch
                                WHERE `status` != "D"
                                AND dxresearch_code IN ${dxcodes}
                                GROUP BY demographic_no

                        ) dxr
                        ON ( d.demographic_no = dxr.demographic_no)


			LEFT JOIN (
                                SELECT m4.demographicNo, m4.dateObserved, m4.dataField
                                FROM measurements m4
                                RIGHT JOIN (
                                        SELECT m1.demographicNo, m1.dateObserved, max(m1.id) AS id
                                        FROM measurements m1
                                        RIGHT JOIN (
                                                SELECT demographicNo, MAX(dateObserved) as dateObserved
                                                FROM measurements
                                                WHERE type = ${a1c}
                                                AND demographicNo > 0
						AND dateObserved > ${lowerLimit.date}

                                                GROUP BY demographicNo
                                        ) m2
                                        ON m1.dateObserved = m2.dateObserved
                                        AND m1.demographicNo = m2.demographicNo
                                        AND m1.type = ${a1c}
                                        GROUP BY m1.demographicNo, m1.dateObserved
                                ) m3
                                ON m4.id = m3.id

                        ) A1C_m
                        ON (d.demographic_no = A1C_m.demographicNo)

			LEFT JOIN (
                                SELECT m4.demographicNo, m4.dateObserved, m4.dataField
				FROM measurements m4	
				RIGHT JOIN (	
                                	SELECT m1.demographicNo, m1.dateObserved, max(m1.id) AS id
                                	FROM measurements m1
                                	RIGHT JOIN (
                                       		SELECT demographicNo, MAX(dateObserved) as dateObserved
                                        	FROM measurements
                                        	WHERE type = ${a1c}
                                        	AND demographicNo > 0
                                        	GROUP BY demographicNo
                                	) m2
                                	ON m1.dateObserved = m2.dateObserved
                                	AND m1.demographicNo = m2.demographicNo
                                	AND m1.type = ${a1c}
					GROUP BY m1.demographicNo, m1.dateObserved
				) m3
				ON m4.id = m3.id

                        ) A1C_m0
                        ON (d.demographic_no = A1C_m0.demographicNo)


 			LEFT JOIN (
                                SELECT m4.demographicNo, m4.dateObserved, m4.dataField
                                FROM measurements m4
                                RIGHT JOIN (
                                        SELECT m.demographicNo, m.dateObserved, max(m.id) AS id
                                        FROM measurements m
                                        RIGHT JOIN (
                                                SELECT m2.demographicNo, m2.type, MAX(m2.dateObserved) AS dateObserved
                                                FROM measurements m2
                                                JOIN measurementsExt me
                                                ON m2.id = me.measurement_id
                                                JOIN measurementMap mm
                                                ON me.val = mm.`name`
                                                WHERE me.keyval = "name"
                                                AND mm.loinc_code IN ${loinc}
                                                AND m2.dateObserved > ${lowerLimit.date}
                                                GROUP BY m2.demographicNo, m2.type
                                        ) m1
                                        ON m.type = m1.type
                                        AND m.dateObserved = m1.dateObserved
                                        AND m.demographicNo = m1.demographicNo
                                        GROUP BY m.demographicNo, m.type, m.dateObserved
                                ) m3
                                ON m4.id = m3.id

                        ) A1C
                        ON (d.demographic_no = A1C.demographicNo)

			-- Last Seen Date
			LEFT JOIN (
        			SELECT a.demographic_no, max(a.appointment_date) as appointment_date
        			FROM appointment a, demographic d
        			WHERE a.demographic_no = d.demographic_no
        			AND d.provider_no = '${provider}'
        			AND ${upperLimit.date} > DATE(a.appointment_date)
        			AND a.status != 'D' AND a.status != 'C' AND a.status != 'N'
        			AND a.demographic_no > 0
        			GROUP BY a.demographic_no
			) app1
			ON (d.demographic_no = app1.demographic_no)

			-- Next Appointment Date
			LEFT JOIN (
        			SELECT a.demographic_no, a.appointment_date
        			FROM appointment a, demographic d
        			WHERE a.demographic_no = d.demographic_no
        			AND d.provider_no = '${provider}'
        			AND DATE(a.appointment_date) >= ${upperLimit.date}
        			AND a.status != 'D' AND a.status != 'C' AND a.status != 'N'
        			AND a.demographic_no > 0
        			GROUP BY a.demographic_no HAVING COUNT(a.demographic_no) > -1
			) app2
			ON (d.demographic_no = app2.demographic_no)

			WHERE d.patient_status = ${active} 
			AND d.provider_no = '${provider}'
			AND d.demographic_no > 0 
                        AND ( (A1C.dataField > ${lowerLimit.a1c} AND ${upperLimit.a1c} >= A1C.dataField) 
				OR (A1C_m.dataField > ${lowerLimit.a1c} AND ${upperLimit.a1c} >= A1C_m.dataField ) )

		</query>
	</drillDownQuery>
	<shared>false</shared>
	<sharedMetricSetName>OntarioMD DM A1C in Range</sharedMetricSetName>
	<sharedMetricDataId>Status</sharedMetricDataId>
	<sharedMappings>
		<sharedMapping fromLabel="HbA1C &lt;= 7%" toLabel="HbA1C &lt;= 7%"/>
		<sharedMapping fromLabel="7% &lt; HbA1C &lt;= 8.5%" toLabel="7% &lt; HbA1C &lt;= 8.5%"/>
		<sharedMapping fromLabel="HbA1C not done &gt; 12 months" toLabel="HbA1C not done &gt; 12 months"/>
		<sharedMapping fromLabel="HbA1C &gt; 8.5%" toLabel="HbA1C &gt; 8.5%"/>
	</sharedMappings>
</indicatorTemplateXML>
