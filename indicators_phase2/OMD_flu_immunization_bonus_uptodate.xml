<?xml version="1.0" encoding="UTF-8"?>
<indicatorTemplateXML>
	<author>Colcamex Resources Inc. for Oscar EMR</author>
	<uid></uid>
	<heading>
		<category>Preventive Health Care</category>
		<subCategory>OMD-Immunization</subCategory>
		<name>OMD-Preventive Care Bonus: Influenza Immunization</name>
		<metricSetName>OntarioMD Care Bonus Influenza Immunization</metricSetName>
		<metricLabel>Influenza up to date</metricLabel>
		<definition>Target population: consists of enrolled patients who are 65 years or older as of December 31st of the fiscal year for which the bonus is being claimed. Service Period: is the current flu season up to January 31st of the year for which the bonus in being claimed. Service Codes reported: G590A, G591A, Q690A, Q691A, and tracking code Q130A</definition>
		<framework>Based on and adapted from CIHI’s 2012 Indicator Technical Specifications (Nov 2012)</framework>
		<frameworkVersion>02-10-2018</frameworkVersion>
		<notes></notes>
	</heading>
	<indicatorQuery>
		<version>02-10-2018</version>
		<params>
			<!-- 
				Required parameter provider. Value options are: 
					[ an individual providerNo, or provider range ] ie: value="370, 1001" 
					"all" ie: value="all" including null.
					"loggedInProvider" ie:
				Default is "loggedInProvider"
				Use this parameter in the query as ${provider}
				This parameter should be used for fetching patient's assigned to a MRP.
				ie: WHERE demographic.provider_no = ${provider}
			-->
			<parameter id="provider" name="provider_no" value="loggedInProvider" />
			<parameter id="prevention" name="prevention_code" value="'Flu'" />
			<parameter id="pstatus" name="Patient Status" value="'AC'" />
			<parameter id="rosterstatus" name="Patient Roster Status" value="'RO'" />	
			<parameter id="ageasof" name="Patient Age As Of Date" value="STR_TO_DATE( CONCAT( '31,', '03,', YEAR( NOW() ) ),'%d,%m,%Y')" />	
			<parameter id="graceperiod" name="Grace Period Months" value="6" />
			<parameter id="servicecode" name="ServiceCode" value="'Q130A'" />
		</params>
		<range>
			<upperLimit id="date" label="January 31" name="Date Upper" value="STR_TO_DATE( CONCAT( '31,', '01,', YEAR( NOW() ) ),'%d,%m,%Y')" />
			<lowerLimit id="date" label="Months History" name="Date Lower" value="6" />	
			<lowerLimit id="fludate" label="From Date" name="Date" value="DATE_FORMAT( CONCAT(IF(9 > MONTH(NOW()), YEAR(NOW()) - 1, YEAR(NOW())),'-',9,'-',30), '%Y-%m-%d' )" />
                        <upperLimit id="fludate" label="To Date" name="Date" value="DATE_FORMAT( CONCAT(IF(9 > MONTH(NOW()), YEAR(NOW()), YEAR(NOW()) + 1),'-',1,'-',31), '%Y-%m-%d' )" />
			<lowerLimit id="refusaldate" label="Refusal From Date" name="Refusal Date" value="DATE_FORMAT( CONCAT(YEAR(NOW())-1,'-',4,'-',1), '%Y-%m-%d' )" />
                        <upperLimit id="refusaldate" label="Refusal To Date" name="Refusal Date" value="DATE_FORMAT( CONCAT(YEAR(NOW()),'-',1,'-',31), '%Y-%m-%d' )" />

			<lowerLimit id="age" label="Patient Age" name="Age Lower" value="65" />
		</range>
		<query>
			<!-- Indicator SQL Query here -->
			SELECT 

				IF ( COUNT(fin.patient) > 0,
					SUM( IF( fin.flu > 0 OR fin.bill > 0, 1, 0) )
				,0 ) AS "% Eligible",
			
				IF ( COUNT(fin.patient) > 0,
					SUM( IF( fin.flu = 0 AND fin.bill = 0, 1, 0) )
				,0 ) AS "% Ineligible"
			
			FROM(
				SELECT 
			
					d.demographic_no AS patient,
					IFNULL( FLU.id, 0 ) AS flu,
					IFNULL( BILL.id, 0 ) AS bill,
					IFNULL( REF.id, 0) AS refusal
			
				FROM demographic d
			
				-- LOOK FOR MOST RECENT FLU IMMUNIZATIONS
				LEFT JOIN (
                                        SELECT p.demographic_no, p.id
                                        FROM preventions p
                                        WHERE p.prevention_type = ${prevention}
                                        AND p.deleted = 0
                                        AND p.refused = 0
                                        AND DATE(p.prevention_date) BETWEEN ${lowerLimit.fludate} AND ${upperLimit.fludate}
                                        GROUP BY p.demographic_no 
                                ) FLU
                                ON (d.demographic_no = FLU.demographic_no )

				-- LOOK FOR Refusal FLU IMMUNIZATIONS
				LEFT JOIN (
                                        SELECT p.demographic_no, p.id
                                        FROM preventions p
                                        WHERE p.prevention_type = ${prevention}
                                        AND p.deleted = 0
                                        AND p.refused = 1
                                        AND DATE(p.prevention_date) BETWEEN ${lowerLimit.refusaldate} AND ${upperLimit.refusaldate}
                                        GROUP BY p.demographic_no 
                                ) REF
                                ON (d.demographic_no = REF.demographic_no )

				-- WITH a tracking code of Q130A between Sep 30 and Jan 31 of the current flu season
				LEFT JOIN (
                                        SELECT boc.id, boc.demographic_no
                                        FROM billing_on_cheader1 boc, billing_on_item boi
                                        WHERE boc.id = boi.ch1_id
                                        AND boc.status != 'D'
                                        AND boi.status != 'D'
                                        AND boi.service_code =  ${servicecode}
                                        AND DATE(boi.service_date) BETWEEN ${lowerLimit.fludate} AND ${upperLimit.fludate}
                                        GROUP BY boc.demographic_no
                                ) BILL
                                ON (d.demographic_no = BILL.demographic_no)

				WHERE d.patient_status = ${pstatus}
				AND d.provider_no = '${provider}'
				AND d.roster_status = ${rosterstatus}
				AND d.demographic_no > 0 
				AND ( FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,"-",d.month_of_birth,"-",d.date_of_birth) ), 
					${upperLimit.date} ) ) / 365.25 ) >= ${lowerLimit.age} )
			
			)fin
		</query>
	</indicatorQuery>
	<drillDownQuery>
		<version>02-10-2018</version>
		<params>
			<parameter id="provider" name="provider_no" value="loggedInProvider" />
			<parameter id="serviceCode" name="billing_service_codes" value="G590A,G591A,Q690A,Q691A" />
			<parameter id="servicecode" name="ServiceCode" value="'Q130A'" />
			<parameter id="prevention" name="prevention_code" value="'Flu'" />
			<parameter id="pstatus" name="Patient Status" value="'AC'" />
			<parameter id="rosterstatus" name="Patient Roster Status" value="'RO'" />	
			<parameter id="ageasof" name="Patient Age As Of Date" value="STR_TO_DATE( CONCAT( '31,', '03,', YEAR( NOW() ) ),'%d,%m,%Y')" />	
			<parameter id="graceperiod" name="Grace Period Months" value="6" />
		</params>
		<range>
			<upperLimit id="date" label="January 31" name="Date Upper" value="STR_TO_DATE( CONCAT( '31,', '01,', YEAR( NOW() ) ),'%d,%m,%Y')" />
			<lowerLimit id="date" label="Months History" name="Date Lower" value="6" />	
			<lowerLimit id="fludate" label="From Date" name="Date" value="DATE_FORMAT( CONCAT(IF(9 > MONTH(NOW()), YEAR(NOW()) - 1, YEAR(NOW())),'-',9,'-',30), '%Y-%m-%d' )" />
                        <upperLimit id="fludate" label="To Date" name="Date" value="DATE_FORMAT( CONCAT(IF(9 > MONTH(NOW()), YEAR(NOW()), YEAR(NOW()) + 1),'-',1,'-',31), '%Y-%m-%d' )" />
			<lowerLimit id="refusaldate" label="Refusal From Date" name="Refusal Date" value="DATE_FORMAT( CONCAT(YEAR(NOW())-1,'-',4,'-',1), '%Y-%m-%d' )" />
                        <upperLimit id="refusaldate" label="Refusal To Date" name="Refusal Date" value="DATE_FORMAT( CONCAT(YEAR(NOW()),'-',1,'-',31), '%Y-%m-%d' )" />

			<lowerLimit id="age" label="Patient Age" name="Age Lower" value="65" />
		</range>
		<displayColumns>
			<column id="demographic" name="d.demographic_no" title="Patient Id" primary="true" />
			<column id="name" name="CONCAT( d.last_name, ', ', d.first_name )" title="Patient Name" primary="false" />
			<column id="dob" name="DATE_FORMAT( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth), '%m-%d-%Y' )" title="Patient DOB (mm-dd-yyyy)" primary="false" />
			<column id="age" name="FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth) ), NOW() ) ) / 365.25 )" title="Patient Age (as of March 31)" primary="false" />
			<column id="phonenumber" name="IFNULL( d.phone, '')" title="Patient Phone Number" primary="false" />
			<column id="trackingservicedate" name="IFNULL(DATE_FORMAT(BILL0.service_date, '%m-%d-%Y' ), '')" title="Date of Most Recent Q130A (mm-dd-yyyy)" primary="false" />
			<column id="lastdate" name="IFNULL( DATE_FORMAT(FLU0.prevention_date, '%m-%d-%Y' ), '')" title="Date of Most Recent Immunization (mm-dd-yyyy)" primary="false" />
<!--
			<column id="status" name="IFNULL(FLU0_R.refused, 'No')" title="Immunization Refusal Status" primary="false" />
			<column id="refusaldate" name="IFNULL(DATE_FORMAT(FLU0_R.prevention_date, '%m-%d-%Y' ), '')" title="Date of Most Recent Refusal (mm-dd-yyyy)" primary="false" />
-->
		</displayColumns>
		<query>
			<!-- Drilldown SQL Query here -->
			SELECT 
				d.demographic_no AS 'Id',
				CONCAT( d.last_name, ", ",d.first_name ) AS 'Name',
				DATE_FORMAT( DATE( CONCAT(d.year_of_birth,"-",d.month_of_birth,"-",d.date_of_birth) ), '%m-%d-%Y' ) AS 'Birth Date (mm-dd-yyyy)',
				ROUND( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,"-",d.month_of_birth,"-",d.date_of_birth) ), NOW() ) ) / 365.25 ) AS 'Age'
			
			FROM demographic d
			
			-- LOOK FOR MOST RECENT FLU IMMUNIZATIONS
			LEFT JOIN (
                                SELECT p.demographic_no, p.id
                                FROM preventions p
                                WHERE p.prevention_type = ${prevention}
                                AND p.deleted = 0
                                AND p.refused = 0
                                AND DATE(p.prevention_date) BETWEEN ${lowerLimit.fludate} AND ${upperLimit.fludate}
                                GROUP BY p.demographic_no
                        ) FLU
                        ON (d.demographic_no = FLU.demographic_no )
			
			LEFT JOIN (
                                SELECT p.demographic_no, MAX(p.prevention_date) AS prevention_date
                                FROM preventions p
                                WHERE p.prevention_type = ${prevention}
                                AND p.deleted = 0
                                AND p.refused = 0
                                GROUP BY demographic_no
                        ) FLU0
                        ON (d.demographic_no = FLU0.demographic_no)

			LEFT JOIN (
                                SELECT p.demographic_no, p.id, MAX(p.prevention_date) AS prevention_date, 'Yes' AS refused
                                FROM preventions p
                                WHERE p.prevention_type = ${prevention}
                                AND p.deleted = 0
                                AND p.refused = 1
                                AND p.demographic_no > 0
                                GROUP BY p.demographic_no 
                        ) FLU0_R
                        ON (d.demographic_no = FLU0_R.demographic_no )

			-- LOOK FOR Refusal FLU IMMUNIZATIONS
			 LEFT JOIN (
                                SELECT p.demographic_no, p.id
                                FROM preventions p
                                WHERE p.prevention_type = ${prevention}
                                AND p.deleted = 0
                                AND p.refused = 1
                                AND DATE(p.prevention_date) BETWEEN ${lowerLimit.refusaldate} AND ${upperLimit.refusaldate}
                                GROUP BY p.demographic_no
                        ) REF
                        ON (d.demographic_no = REF.demographic_no )

			-- TRACKING/EXCLUSION CODE USED.
			-- WITH a tracking code of Q130A between Sep 30 and Jan 31 of the current flu season
                        LEFT JOIN (
                                SELECT boc.id, boc.demographic_no
                                FROM billing_on_cheader1 boc, billing_on_item boi
                                WHERE boc.id = boi.ch1_id
                                AND boc.status != 'D'
                                AND boi.status != 'D'
                                AND boi.service_code =  ${servicecode}
                                AND DATE(boi.service_date) BETWEEN ${lowerLimit.fludate} AND ${upperLimit.fludate}
                                GROUP BY boc.demographic_no
                        ) BILL
                        ON (d.demographic_no = BILL.demographic_no)

			LEFT JOIN (
                                SELECT boc.id, boc.demographic_no, MAX(boi.service_date) AS service_date
                                FROM billing_on_cheader1 boc, billing_on_item boi
                                WHERE boc.id = boi.ch1_id
                                AND boc.status != 'D'
                                AND boi.status != 'D'
                                AND boi.service_code =  ${servicecode}
                                GROUP BY boc.demographic_no
                        ) BILL0
                        ON (d.demographic_no = BILL0.demographic_no)

			WHERE d.patient_status = ${pstatus}
			AND d.provider_no = '${provider}'
			AND d.roster_status = ${rosterstatus}
			AND d.demographic_no > 0 
			AND ( FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,"-",d.month_of_birth,"-",d.date_of_birth) ), 
				${upperLimit.date} ) ) / 365.25 ) >= ${lowerLimit.age} )				
                        AND (FLU.id > 0 OR BILL.id > 0)

		</query>
	</drillDownQuery>
	<shared>false</shared>
	<sharedMetricSetName>OntarioMD Care Bonus Influenza Immunization</sharedMetricSetName>
	<sharedMetricDataId>Status</sharedMetricDataId>
	<sharedMappings>
		<sharedMapping fromLabel="% Eligible" toLabel="Influenza up to date"/>
		<sharedMapping fromLabel="% Ineligible" toLabel="Influenza overdue"/>
	</sharedMappings>
</indicatorTemplateXML>
