<?xml version="1.0" encoding="UTF-8"?>
<indicatorTemplateXML>
	<author>Trinity Healthcare Technologies</author>
	<uid></uid>
	<heading>
		<category>OntarioMD Prac. Management</category>
		<subCategory>Patient Status</subCategory>
		<name>Patient Status</name>
		<metricSetName>Home - Patient status</metricSetName>
                <metricLabel>Active, not seen in 2+ yrs</metricLabel>
		<definition>% of patients (Rostered and non-Rostered) in the practice based on their status, including last visit.</definition>
		<framework>OSCAR EMR</framework>
		<frameworkVersion>02-14-2018</frameworkVersion>
		<notes></notes>
	</heading>
	<indicatorQuery>
		<version>02-14-2018</version>
		<params>
			<!-- 
				Use this parameter in the query as ${provider}
				This parameter should be used for fetching patient's assigned to a MRP.
				ie: WHERE demographic.provider_no = ${provider}
			-->
			<parameter id="provider" name="provider_no" value="loggedInProvider" />
			<parameter id="pstatus" name="Patient Status" value="'AC'" />
		</params>
		<query>
			<!-- Indicator SQL Query here -->
			SELECT

				-- PERCENT OF TOTAL PATIENTS NOTED AS ACTIVE SEEN WITHIN 12 MONTHS (latest visit &lt;= 12 months)
				IF( COUNT(fin.patient) > 0, 
					SUM( IF( fin.pt_status = 'AC' and DATEDIFF(CURDATE(),fin.last_visit) &lt;= 365  , 1, 0) )
				, 0 ) AS "% Active, Seen within 12 months",
			
				-- PERCENT OF TOTAL PATIENTS NOTED AS ACTIVE SEEN 12-24 MONTHS AGO (latest visit &gt; 12 months and &lt; 24 months)
				IF( COUNT(fin.patient) > 0, 
					 SUM( IF( fin.pt_status = 'AC' and DATEDIFF(CURDATE(),fin.last_visit) > 365 and DATEDIFF(CURDATE(),fin.last_visit) &lt;= 730  , 1, 0) )
				, 0 ) AS "% Active, Seen 12-24 months ago",
				
				-- PERCENT OF TOTAL PATIENTS NOTED AS ACTIVE NOT SEEN WITHIN 24 MONTHS (latest visit &gt; 24 months)
				IF( COUNT(fin.patient) > 0, 
					SUM( IF( fin.pt_status = 'AC' and (DATEDIFF(CURDATE(),fin.last_visit) > 730 OR fin.last_visit IS NULL)  , 1, 0) )
				, 0 ) AS "% Active, not seen in 24 months",
				
				-- PERCENT OF TOTAL PATIENTS NOTED AS INACTIVE
				IF( COUNT(fin.patient) > 0, 
					 SUM( IF( fin.pt_status = 'IN' , 1, 0) )
				, 0 ) AS "% Inactive",
				
				-- PERCENT OF TOTAL PATIENTS NOTED AS DECEASED
				IF( COUNT(fin.patient) > 0, 
					  SUM( IF( fin.pt_status = 'DE' , 1, 0) )
				, 0 ) AS "% Deceased",
				
				-- PERCENT OF TOTAL PATIENTS NOTED AS OTHER (not active, not inactive, not deceased, not deleted?)
				IF( COUNT(fin.patient) > 0, 
					  SUM( IF( fin.pt_status NOT IN ('AC','DE','IN') , 1, 0) )
				, 0 ) AS "% Other Patient Status"
				

			FROM (
				SELECT
					d.demographic_no AS patient,
					d.patient_status AS pt_status,
					app1.appointment_date as last_visit
				FROM demographic d
			
				
				-- Notations for last visit in appointments
				LEFT JOIN ( 
					SELECT a.demographic_no, max(a.appointment_date) as appointment_date
        				FROM appointment a, demographic d
        				WHERE a.demographic_no = d.demographic_no
        				AND d.provider_no = '${provider}'
        				AND NOW() >= DATE(a.appointment_date)
        				AND a.status != 'D' AND a.status != 'C' AND a.status != 'N'
        				AND a.demographic_no > 0
        				GROUP BY a.demographic_no
				) app1 
				ON (d.demographic_no = app1.demographic_no)

				WHERE d.provider_no = '${provider}'
			) fin
		</query>
	</indicatorQuery>
	<drillDownQuery>
		<version>02-14-2018</version>
		<params>
			<parameter id="provider" name="provider_no" value="loggedInProvider" />	
		</params>
		<displayColumns>
			<column id="demographic" name="d.demographic_no" title="Patient Id" primary="true" />
			<column id="name" name="CONCAT( d.last_name, ', ', d.first_name )" title="Patient Name" primary="false" />
			<column id="dob" name="DATE_FORMAT( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth), '%m-%d-%Y' )" title="Patient DOB (mm-dd-yyyy)" primary="false" />
			<column id="age" name="FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth) ), NOW() ) ) / 365.25 )" title="Patient Age" primary="false" />	
                        <column id="phonenumber" name="IFNULL( d.phone, '')" title="Patient Phone Number" primary="false" />
			<column id="rosterStatus" name="IFNULL( d.roster_status, '') " title="Roster Status" primary="false" />
			<column id="rosteredDate" name="IF(d.roster_date IS NOT NULL, DATE_FORMAT( d.roster_date, '%m-%d-%Y'), '')" title="Rostered Date (mm-dd-yyyy)" primary="false" />
			<column id="lastVisit" name="IF(app1.appointment_date IS NOT NULL, DATE_FORMAT( app1.appointment_date, '%m-%d-%Y'),'')" title="Date of Last Visit (mm-dd-yyyy)" primary="false" />
		</displayColumns>
		<exportColumns>
			<column id="demographic" name="d.demographic_no" title="Patient Id" primary="true" />
			<column id="name" name="CONCAT( d.last_name, ', ', d.first_name )" title="Patient Name" primary="false" />
			<column id="dob" name="DATE_FORMAT( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth), '%m-%d-%Y' )" title="Patient DOB (mm-dd-yyyy)" primary="false" />
                        <column id="age" name="FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth) ), NOW() ) ) / 365.25 )" title="Patient Age" primary="false" />   
                        <column id="phonenumber" name="IFNULL( d.phone, '')" title="Patient Phone Number" primary="false" />
                        <column id="rosterStatus" name="IFNULL( d.roster_status, '') " title="Roster Status" primary="false" />
                        <column id="rosteredDate" name="IF(d.roster_date IS NOT NULL, DATE_FORMAT( d.roster_date, '%m-%d-%Y'), '')" title="Rostered Date (mm-dd-yyyy)" primary="false" />
			<column id="lastVisit" name="IF(app1.appointment_date IS NOT NULL, DATE_FORMAT( app1.appointment_date, '%m-%d-%Y'),'')" title="Date of Last Visit (mm-dd-yyyy)" primary="false" />

		</exportColumns>
		<query>
			<!-- Drilldown SQL Query here -->
			
			SELECT
				d.*,
				d.patient_status AS pt_status
				FROM demographic d
			
				
				-- Notations for last visit in appointments
				LEFT JOIN (
        				SELECT a.demographic_no, max(a.appointment_date) as appointment_date
        				FROM appointment a, demographic d
        				WHERE a.demographic_no = d.demographic_no
        				AND d.provider_no = '${provider}'
        				AND NOW() > DATE(a.appointment_date)
        				AND a.status != 'D' AND a.status != 'C' AND a.status != 'N'
        				AND a.demographic_no > 0
        				GROUP BY a.demographic_no
				) app1 
				ON (d.demographic_no = app1.demographic_no)

				WHERE d.patient_status = 'AC'
				AND d.provider_no = '${provider}'
                                AND ( DATEDIFF(CURDATE(), app1.appointment_date) > 730  OR app1.appointment_date IS NULL )


		</query>
	</drillDownQuery>
	<shared>false</shared>
	<sharedMetricSetName>Home - Patient status</sharedMetricSetName>
	<sharedMetricDataId>Status</sharedMetricDataId>
	<sharedMappings>
		<sharedMapping fromLabel="% Active, Seen within 12 months" toLabel="Active, seen this year"/>
		<sharedMapping fromLabel="% Active, Seen 12-24 months ago" toLabel="Active, seen within 1-2 yrs"/>
		<sharedMapping fromLabel="% Active, not seen in 24 months" toLabel="Active, not seen in 2+ yrs"/>
		<sharedMapping fromLabel="% Inactive" toLabel="Inactive"/>
		<sharedMapping fromLabel="% Deceased" toLabel="Deceased"/>
		<sharedMapping fromLabel="% Other Patient Status" toLabel="Other"/>
	</sharedMappings>	
</indicatorTemplateXML>
