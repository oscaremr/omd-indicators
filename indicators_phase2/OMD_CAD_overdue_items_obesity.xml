<?xml version="1.0" encoding="UTF-8"?>
<indicatorTemplateXML>
	<author>Trinity Healthcare Technologies for Oscar EMR</author>
	<uid></uid>
	<heading>
		<category>CDM</category>
		<subCategory>OMD-Coronary Artery Disease Overdue Items</subCategory>
		<name>OMD-Coronary Artery Disease Overdue Items</name>
		<metricSetName>OntarioMD CAD Overdue items</metricSetName>
		<metricLabel>Obesity screen overdue</metricLabel>
		<definition>% of patients age 18 and older, with coronary artery disease (CAD) who received testing for any of the following:
					-Lipid profile screening (ever received a test)
					-Blood pressure measurement (in last 12 months)
					-Obesity/overweight screening (in last 12 months)
		</definition>
		<framework>Based on and adapted from CIHI’s 2012 Indicator Technical Specifications (Nov 2012)</framework>
		<frameworkVersion>02-20-2018</frameworkVersion>
		<notes></notes>
	</heading>
	<indicatorQuery>
		<version>02-20-2018</version>
		<params>
			<!-- 
				Required parameter provider. Value options are: 
					[ an individual providerNo, or provider range ] ie: value="370, 1001" 
					"all" ie: value="all" including null.
					"loggedInProvider" ie:
				Default is "loggedInProvider"
				Use this parameter in the query as ${provider}
				This parameter should be used for fetching patient's assigned to a MRP.
				ie: WHERE demographic.provider_no = ${provider}
			-->
			<parameter id="provider" name="provider_no" value="loggedInProvider" />
			<parameter id="lipidLoinc" name="LOINC List" value="22748-8,14646-4,14647-2,14927-8" />
			<parameter id="cadMeasurement" name="CAD Measurement" value="TCHD,TG,LDL,TCHL,HDL" />
			<parameter id="bloodPressure" name="Blood Pressure" value="'BP'" />
			<parameter id="bmi" name="Body Mass Index" value="'BMI'" />
			<parameter id="wt" name="Weight" value="'WT'" />
			<parameter id="ht" name="Height" value="'HT'" />
			<parameter id="wc" name="Waist Circumference" value="'WAIS'" />
			<parameter id="pstatus" name="Patient Status" value="'AC'" />		
			<parameter id="dxcodes" name="Dx Research Codes" value="410,411,412,413,414,4292,I20,I25,D3-13040,D3-10030 IHD,D3-14016,D3-14017,414545008,413439005,413838009,194828000" />
			<parameter id="dxcode" name="Dx Code Range" value="'414.%'" />	
		</params>
		<range>
			<upperLimit id="date" label="Date Today" name="Date" value="NOW()" />
			<lowerLimit id="date" label="12 Months History" name="Date" value="DATE_SUB( NOW(), INTERVAL 12 MONTH )" />
			
			<lowerLimit id="age" label="Minimum Age" name="Age" value="18" />
		</range>
		<query>
			<!-- Indicator SQL Query here -->
			SELECT 

				IF ( COUNT(fin.patient) > 0,
				 SUM( IF( fin.dr > 0 AND fin.bp = 0 , 1, 0 ) )
				,0 ) AS '% BP overdue',
				
				IF ( COUNT(fin.patient) > 0,
				 SUM( IF( fin.dr > 0 AND fin.lipid = 0 AND fin.lipid_m = 0 , 1, 0 ) ) 
				,0 ) AS '% Lipid profile not done',
				
				IF ( COUNT(fin.patient) > 0,
				 SUM( IF( fin.dr > 0 AND fin.bmi = 0 AND ( fin.ht = 0 OR (fin.ht > 0 AND (fin.wt = 0 AND fin.wc = 0 ))) , 1, 0 ) ) 
				,0 ) AS '% Obesity screen overdue'
			
			FROM (
				SELECT 
					d.demographic_no AS patient,
					IFNULL(dxr.dxresearch_no, 0) AS dr,
					IFNULL(BP.id, 0) AS bp,
					IFNULL(LIPID.id, 0) AS lipid,
					IFNULL(LIPID_m.id, 0) AS lipid_m,
					IFNULL(BMI.id, 0) AS bmi,
					IFNULL(HT.id, 0) AS ht,
					IFNULL(WT.id, 0) AS wt,
					IFNULL(WC.id, 0) AS wc

				FROM demographic d 

				INNER JOIN (
					SELECT dxresearch_no, demographic_no
                                        FROM dxresearch
                                        WHERE demographic_no > 0
					AND `status` != "D"
					AND ( dxresearch_code IN ${dxcodes} OR dxresearch_code LIKE ${dxcode} )
                                        GROUP BY demographic_no HAVING COUNT(demographic_no) > -1
				) dxr 
				ON ( d.demographic_no = dxr.demographic_no) 
			
				-- Lipid Screening
				LEFT JOIN (
                                        SELECT m2.demographicNo, m2.id
                                        FROM measurements m2
                                        JOIN measurementsExt me
                                        ON m2.id = me.measurement_id
                                        JOIN measurementMap mm
                                        ON me.val = mm.`name`
                                        WHERE me.keyval = "name"
                                        AND mm.loinc_code IN ${lipidLoinc}
                                        GROUP BY m2.demographicNo

                                ) LIPID
                                ON (d.demographic_no = LIPID.demographicNo)

				LEFT JOIN (
                                        SELECT id, dataField, demographicNo
                                        FROM measurements
                                        WHERE type in ${cadMeasurement}
                                        AND demographicNo > 0
                                        GROUP BY demographicNo HAVING COUNT(demographicNo) > -1
                                ) LIPID_m
                                ON (d.demographic_no = LIPID_m.demographicNo)	
			
				-- Blood Pressure
				LEFT JOIN (
					SELECT id, dataField, demographicNo 
					FROM measurements 
					WHERE type = ${bloodPressure}
					AND DATE(dateObserved) > ${lowerLimit.date} 
					AND demographicNo > 0 
					GROUP BY demographicNo HAVING COUNT(demographicNo) > -1
				) BP
				ON (d.demographic_no = BP.demographicNo)
			
				-- BMI Screening
				-- Look for BMI index or if the height and weight was measured.
				LEFT JOIN ( 
					SELECT id, dataField, demographicNo 
					FROM measurements 
					WHERE type = ${bmi} 
					AND DATE(dateObserved) > ${lowerLimit.date} 
					AND demographicNo > 0 
					GROUP BY demographicNo HAVING COUNT(demographicNo) > -1
				) BMI 
				ON (d.demographic_no = BMI.demographicNo)
			
				-- HEIGHT
				LEFT JOIN ( 
					SELECT id, dataField, demographicNo 
					FROM measurements 
					WHERE type = ${ht} 
					AND DATE(dateObserved) > ${lowerLimit.date} 
					AND demographicNo > 0 
					GROUP BY demographicNo HAVING COUNT(demographicNo) > -1
				) HT 
				ON (d.demographic_no = HT.demographicNo)
				
				-- WEIGHT
				LEFT JOIN ( 
					SELECT id, dataField, demographicNo 
					FROM measurements 
					WHERE type = ${wt} 
					AND DATE(dateObserved) > ${lowerLimit.date} 
					AND demographicNo > 0 
					GROUP BY demographicNo HAVING COUNT(demographicNo) > -1
				) WT 
				ON (d.demographic_no = WT.demographicNo)
				
				-- WAIST CIRCUMFERENCE
				LEFT JOIN ( 
					SELECT id, dataField, demographicNo 
					FROM measurements 
					WHERE type = ${wc} 
					AND DATE(dateObserved) > ${lowerLimit.date} 
					AND demographicNo > 0 
					GROUP BY demographicNo HAVING COUNT(demographicNo) > -1
				) WC 
				ON (d.demographic_no = WC.demographicNo)
				
				WHERE d.patient_status = ${pstatus} 
				AND provider_no = '${provider}'
				AND d.demographic_no > 0 
				AND ( FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,"-",d.month_of_birth,"-",d.date_of_birth) ), 
					NOW() ) ) / 365.25 ) >= ${lowerLimit.age} ) 
				GROUP BY d.demographic_no HAVING COUNT(d.demographic_no) > -1
			) fin
		</query>
	</indicatorQuery>
	<drillDownQuery>
		<version>02-20-2018</version>
		<params>
			<parameter id="provider" name="provider_no" value="loggedInProvider" />
			<parameter id="lipidLoinc" name="LOINC List" value="22748-8,14646-4,14647-2,14927-8" />
			<parameter id="cadMeasurement" name="CAD Measurement" value="TCHD,TG,LDL,TCHL,HDL" />
			<parameter id="bloodPressure" name="Blood Pressure" value="'BP'" />
			<parameter id="bmi" name="Body Mass Index" value="'BMI'" />
			<parameter id="wt" name="Weight" value="'WT'" />
			<parameter id="ht" name="Height" value="'HT'" />
			<parameter id="wc" name="Waist Circumference" value="'WAIS'" />
			<parameter id="pstatus" name="Patient Status" value="'AC'" />	
                        <parameter id="dxcodes" name="Dx Research Codes" value="410,411,412,413,414,4292,I20,I25,D3-13040,D3-10030 IHD,D3-14016,D3-14017,414545008,413439005,413838009,194828000" />
			<parameter id="dxcode" name="Dx Code Range" value="'414.%'" />
			<parameter id="sharedMetricLabel" name="sharedMetricLabel" value="sharedMetricLabel" />
		</params>
		<range>
			<upperLimit id="date" label="Date Today" name="Date" value="NOW()" />
			<lowerLimit id="date" label="12 Months History" name="Date" value="DATE_SUB( NOW(), INTERVAL 12 MONTH )" />
			
			<lowerLimit id="age" label="Minimum Age" name="Age" value="18" />
		</range>
		<displayColumns>
			<column id="demographic" name="d.demographic_no" title="Patient Id" primary="true" />
			<column id="name" name="CONCAT( d.last_name, ', ', d.first_name )" title="Patient Name" primary="false" />
			<column id="dob" name="DATE_FORMAT( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth), '%m-%d-%Y' )" title="Patient DOB (mm-dd-yyyy)" primary="false" />
			<column id="age" name="FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth) ), NOW() ) ) / 365.25 )" title="Patient Age" primary="false" />
			<column id="phonenumber" name="IFNULL( d.phone, '')" title="Patient Phone Number" primary="false" />
<!--
			<column id="lipidtype" name="IFNULL( LIPID_m.type, ' ')" title="Most Recent LIPID Type" primary="false" />
			<column id="lipid" name="IFNULL( LIPID_m.dataField, ' ')" title="Most Recent LIPID Value" primary="false" />
			<column id="lipiddate" name="IFNULL( DATE_FORMAT( LIPID_m.dateObserved, '%m-%d-%Y' ), ' ')" title="Date of Most Recent LIPID (mm-dd-yyyy)" primary="false" />
			<column id="bp" title="Most Recent BP" name="IFNULL( BP0.dataField, ' ')" primary="false" />
			<column id="bpdate" title="Date of Most Recent BP (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( BP0.dateObserved, '%m-%d-%Y' ), ' ')" primary="false" />
-->
			<column id="bmi" title="Most Recent BMI" name="IFNULL( BMI0.dataField, ' ')" primary="false" />
			<column id="bmidate" title="Date of Most Recent BMI (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( BMI0.dateObserved, '%m-%d-%Y' ), ' ')" primary="false" />
			<column id="ht" title="Most Recent Height" name="IFNULL( HT0.dataField, ' ')" primary="false" />
			<column id="htdate" title="Date of Most Recent Height (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( HT0.dateObserved, '%m-%d-%Y' ), ' ')" primary="false" />
			<column id="wt" title="Most Recent Weight" name="IFNULL( WT0.dataField, ' ')" primary="false" />
			<column id="wtdate" title="Date of Most Recent Weight (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( WT0.dateObserved, '%m-%d-%Y' ), ' ')" primary="false" />
			<column id="wc" title="Most Recent Waist Circumference" name="IFNULL( WC0.dataField, ' ')" primary="false" />
			<column id="wcdate" title="Date of Most Recent Waist Circumference (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( WC0.dateObserved, '%m-%d-%Y' ), ' ')" primary="false" />
			<column id="lastappdate" name="IFNULL(  DATE_FORMAT(app1.appointment_date, '%m-%d-%Y' ), '')" title="Last Seen Date (mm-dd-yyyy)" primary="false" />
            		<column id="nextappdate" name="IFNULL(  DATE_FORMAT(app2.appointment_date, '%m-%d-%Y' ), '')" title="Next Appointment Date (mm-dd-yyyy)" primary="false" />
<!--
			<column id="dxcode" title="Dx System/Code" name="CONCAT(MIN(dxr.coding_system),'/',MIN(dxr.dxresearch_code),IF( MIN(dxr.dxresearch_code) != MAX(dxr.dxresearch_code),CONCAT( ' &amp; ', MAX(dxr.coding_system), '/', MAX(dxr.dxresearch_code) ),'') )" primary="false" />
-->
		</displayColumns>
		<exportColumns>
			<column id="demographic" name="d.demographic_no" title="Patient Id" primary="true" />
			<column id="firstName" name="d.first_name" title="First Name" primary="false" />
			<column id="lastName" name="d.last_name" title="Last Name" primary="false" />
			<column id="dob" name="DATE_FORMAT( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth), '%m-%d-%Y' )" title="Patient DOB (mm-dd-yyyy)" primary="false" />
                        <column id="age" name="FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth) ), NOW() ) ) / 365.25 )" title="Patient Age" primary="false" />
                        <column id="phonenumber" name="IFNULL( d.phone, '')" title="Patient Phone Number" primary="false" />
<!--
			<column id="lipidtype" name="IFNULL( LIPID_m.type, ' ')" title="Most Recent LIPID Type" primary="false" />
                        <column id="lipid" name="IFNULL( LIPID_m.dataField, ' ')" title="Most Recent LIPID Value" primary="false" />
                        <column id="lipiddate" name="IFNULL( DATE_FORMAT( LIPID_m.dateObserved, '%m-%d-%Y' ), ' ')" title="Date of Most Recent LIPID (mm-dd-yyyy)" primary="false" />
                        <column id="bp" title="Most Recent BP" name="IFNULL( BP0.dataField, ' ')" primary="false" />
                        <column id="bpdate" title="Date of Most Recent BP (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( BP0.dateObserved, '%m-%d-%Y' ), ' ')" primary="false" />
-->
                        <column id="bmi" title="Most Recent BMI" name="IFNULL( BMI0.dataField, ' ')" primary="false" />
                        <column id="bmidate" title="Date of Most Recent BMI (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( BMI0.dateObserved, '%m-%d-%Y' ), ' ')" primary="false" />
                        <column id="ht" title="Most Recent Height" name="IFNULL( HT0.dataField, ' ')" primary="false" />
                        <column id="htdate" title="Date of Most Recent Height (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( HT0.dateObserved, '%m-%d-%Y' ), ' ')" primary="false" />
                        <column id="wt" title="Most Recent Weight" name="IFNULL( WT0.dataField, ' ')" primary="false" />
                        <column id="wtdate" title="Date of Most Recent Weight (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( WT0.dateObserved, '%m-%d-%Y' ), ' ')" primary="false" />
                        <column id="wc" title="Most Recent Waist Circumference" name="IFNULL( WC0.dataField, ' ')" primary="false" />
                        <column id="wcdate" title="Date of Most Recent Waist Circumference (mm-dd-yyyy)" name="IFNULL( DATE_FORMAT( WC0.dateObserved, '%m-%d-%Y' ), ' ')" primary="false" />
                        <column id="lastappdate" name="IFNULL(  DATE_FORMAT(app1.appointment_date, '%m-%d-%Y' ), '')" title="Last Seen Date (mm-dd-yyyy)" primary="false" />
                        <column id="nextappdate" name="IFNULL(  DATE_FORMAT(app2.appointment_date, '%m-%d-%Y' ), '')" title="Next Appointment Date (mm-dd-yyyy)" primary="false" />

<!--
			<column id="dxcode" title="Dx System/Code" name="CONCAT(MIN(dxr.coding_system),'/',MIN(dxr.dxresearch_code),IF( MIN(dxr.dxresearch_code) != MAX(dxr.dxresearch_code),CONCAT( ' - ', MAX(dxr.coding_system), '/', MAX(dxr.dxresearch_code) ),'') )" primary="false" />
-->
		</exportColumns>
		<query>
			<!-- Drilldown SQL Query here -->
			SELECT 
			*
			FROM demographic d 
			INNER JOIN (
				SELECT dxresearch_no, demographic_no
                                FROM dxresearch
                                WHERE demographic_no > 0
				AND `status` != "D"
				AND ( dxresearch_code IN ${dxcodes} OR dxresearch_code LIKE ${dxcode} )
				GROUP BY demographic_no HAVING COUNT(demographic_no) > -1
			) dxr  
			ON ( d.demographic_no = dxr.demographic_no) 
		
			-- Lipid Screening

			-- Blood Pressure
			LEFT JOIN (
                                SELECT m4.id, m4.dataField, m4.demographicNo, m4.dateObserved
                                FROM measurements m4
                                RIGHT JOIN (
                                	SELECT MAX(m1.id) AS id, m1.dataField, m1.demographicNo, m1.dateObserved
                                	FROM measurements m1
                                	RIGHT JOIN (
                                        	SELECT m.demographicNo, m.type, MAX(m.dateObserved) as dateObserved
                                        	FROM measurements m, demographic d
                                        	WHERE m.demographicNo = d.demographic_no
                                        	AND d.provider_no = '${provider}'
                                        	AND m.type = ${bloodPressure}
                                        	AND m.demographicNo > 0
                                        	GROUP BY m.demographicNo
                                	) m2
                                	ON m1.dateObserved = m2.dateObserved
                                	AND m1.demographicNo = m2.demographicNo
                                	AND m1.type = ${bloodPressure}
				 	GROUP BY m1.demographicNo, m1.type, m1.dateObserved	
				) m3
                                ON m4.id = m3.id

                        ) BP0
                        ON (d.demographic_no = BP0.demographicNo)


			-- BMI Screening
			-- Look for BMI index or if the height and weight was measured.
			LEFT JOIN (
				SELECT m4.id, m4.dataField, m4.demographicNo, m4.dateObserved
                                FROM measurements m4
                                RIGHT JOIN (
                                	SELECT MAX(m1.id) AS id, m1.demographicNo, m1.type, m1.dateObserved
                                	FROM measurements m1
                                	RIGHT JOIN (
                                        	SELECT m.demographicNo, m.type, MAX(m.dateObserved) as dateObserved
                                        	FROM measurements m, demographic d
                                        	WHERE m.demographicNo = d.demographic_no
                                        	AND d.provider_no = '${provider}'
                                        	AND m.type = ${bmi}
                                        	AND m.demographicNo > 0
                                        	GROUP BY m.demographicNo
                                	) m2
                                	ON m1.dateObserved = m2.dateObserved
                                	AND m1.demographicNo = m2.demographicNo
                                	AND m1.type = ${bmi}
					GROUP BY m1.demographicNo, m1.type, m1.dateObserved	
				) m3
                                ON m4.id = m3.id

                        ) BMI0
                        ON (d.demographic_no = BMI0.demographicNo)
	
		
			-- HEIGHT
			LEFT JOIN (
				SELECT m4.id, m4.dataField, m4.demographicNo, m4.dateObserved
                                FROM measurements m4
                                RIGHT JOIN (
                                	SELECT MAX(m1.id) AS id, m1.type, m1.demographicNo, m1.dateObserved
                                	FROM measurements m1
                                	RIGHT JOIN (
                                        	SELECT m.demographicNo, m.type, MAX(m.dateObserved) as dateObserved
                                        	FROM measurements m, demographic d
                                        	WHERE m.demographicNo = d.demographic_no
                                        	AND d.provider_no = '${provider}'
                                        	AND m.type = ${ht}
                                        	AND m.demographicNo > 0
                                        	GROUP BY m.demographicNo
                                	) m2
                                	ON m1.dateObserved = m2.dateObserved
                                	AND m1.demographicNo = m2.demographicNo
                                	AND m1.type = ${ht}
					GROUP BY m1.demographicNo, m1.type, m1.dateObserved
				) m3
                                ON m4.id = m3.id
                        ) HT0
                        ON (d.demographic_no = HT0.demographicNo)


			-- WEIGHT
			LEFT JOIN (
				SELECT m4.id, m4.dataField, m4.demographicNo, m4.dateObserved
                                FROM measurements m4
                                RIGHT JOIN (
                                	SELECT MAX(m1.id) AS id, m1.type, m1.demographicNo, m1.dateObserved
                                	FROM measurements m1
                                	RIGHT JOIN (
                                        	SELECT m.demographicNo, m.type, MAX(m.dateObserved) as dateObserved
                                        	FROM measurements m, demographic d
                                        	WHERE m.demographicNo = d.demographic_no
                                        	AND d.provider_no = '${provider}'
                                        	AND m.type = ${wt}
                                        	AND m.demographicNo > 0
                                        	GROUP BY m.demographicNo
                                	) m2
                                	ON m1.dateObserved = m2.dateObserved
                                	AND m1.demographicNo = m2.demographicNo
                                	AND m1.type = ${wt}
				        GROUP BY m1.demographicNo, m1.type, m1.dateObserved
                                ) m3
                                ON m4.id = m3.id
                        ) WT0
                        ON (d.demographic_no = WT0.demographicNo)
	

			-- WAIST CIRCUMFERENCE
			LEFT JOIN (
				SELECT m4.id, m4.dataField, m4.demographicNo, m4.dateObserved
                                FROM measurements m4
                                RIGHT JOIN (
                                	SELECT MAX(m1.id) AS id, m1.type, m1.demographicNo, m1.dateObserved
                                	FROM measurements m1
                                	RIGHT JOIN (
                                        	SELECT m.demographicNo, m.type, MAX(m.dateObserved) as dateObserved
                                        	FROM measurements m, demographic d
                                        	WHERE m.demographicNo = d.demographic_no
                                        	AND d.provider_no = '${provider}'
                                        	AND m.type = ${wc}
                                        	AND m.demographicNo > 0
                                        	GROUP BY m.demographicNo
                                	) m2
                                	ON m1.dateObserved = m2.dateObserved
                                	AND m1.demographicNo = m2.demographicNo
                                	AND m1.type = ${wc}
					GROUP BY m1.demographicNo, m1.type, m1.dateObserved
				) m3
                                ON m4.id = m3.id
                        ) WC0
                        ON (d.demographic_no = WC0.demographicNo)
	
			
			-- Date of most recent appointment
			LEFT JOIN (
                                SELECT a.demographic_no, max(a.appointment_date) as appointment_date
                                FROM appointment a, demographic d
                                WHERE a.demographic_no = d.demographic_no
                                AND d.provider_no = '${provider}'
                                AND ${upperLimit.date} > DATE(a.appointment_date)
                                AND a.status != 'D' AND a.status != 'C' AND a.status != 'N'
                                AND a.demographic_no > 0
                                GROUP BY a.demographic_no
                        ) app1
                        ON (d.demographic_no = app1.demographic_no)


                        -- Next Appointment Date
                        LEFT JOIN (
                                SELECT a.demographic_no, a.appointment_date
                                FROM appointment a, demographic d
                                WHERE a.demographic_no = d.demographic_no
                                AND d.provider_no = '${provider}'
                                AND DATE(a.appointment_date) >= ${upperLimit.date}
                                AND a.status != 'D' AND a.status != 'C' AND a.status != 'N'
                                AND a.demographic_no > 0
                                GROUP BY a.demographic_no HAVING COUNT(a.demographic_no) > -1
                        ) app2
                        ON (d.demographic_no = app2.demographic_no)

			WHERE d.patient_status = ${pstatus} 
			AND provider_no = '${provider}'
			AND d.demographic_no > 0 
			AND ( FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,"-",d.month_of_birth,"-",d.date_of_birth) ), 
				NOW() ) ) / 365.25 ) >= ${lowerLimit.age} ) 
			AND dxr.dxresearch_no > 0 AND (BMI0.id IS NULL OR ${lowerLimit.date} >= BMI0.dateObserved) AND ( HT0.id IS NULL OR ${lowerLimit.date} >= HT0.dateObserved OR (HT0.dateObserved > ${lowerLimit.date} AND ( (WT0.id IS NULL OR ${lowerLimit.date} >= WT0.dateObserved) AND (WC0.id IS NULL OR ${lowerLimit.date} >= WC0.dateObserved) ) ) )

			GROUP BY d.demographic_no HAVING COUNT(d.demographic_no) > -1
			<!--
			AND IF( "${sharedMetricLabel}" = "1+ test up to date", LIPID.dataField > 0 OR BP.dataField > 0 OR BMI.dataField > 0,  
			    IF( "${sharedMetricLabel}" = "All tests overdue", LIPID.dataField = 0 OR BP.dataField = 0 OR BMI.dataField = 0, 1 = 1 ) )
			-->
		</query>
	</drillDownQuery>
	<shared>false</shared>
	<sharedMetricSetName>OntarioMD CAD Overdue items</sharedMetricSetName>
	<sharedMetricDataId>Status</sharedMetricDataId>
	<sharedMappings>
		<sharedMapping fromLabel="% BP overdue" toLabel="BP overdue"/>
		<sharedMapping fromLabel="% Lipid profile not done" toLabel="Lipid profile not done"/>
		<sharedMapping fromLabel="% Obesity screen overdue" toLabel="Obesity screen overdue"/>
	</sharedMappings>
</indicatorTemplateXML>
