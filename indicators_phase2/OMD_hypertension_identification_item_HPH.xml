<?xml version="1.0" encoding="UTF-8"?>
<indicatorTemplateXML>
	<author>Trinity Healthcare Technologies for Oscar EMR</author>
	<uid></uid>
	<heading>
		<category>CDM</category>
		<subCategory>OMD-Hypertension</subCategory>
		<name>OMD-Hypertension Identification</name>
		<metricSetName>OntarioMD HTN Identification</metricSetName>
		<metricLabel>HTN Coded in PROB or HPH</metricLabel>
		<definition># of active patients with hypertension [ICD9: 401,402,403,404,405] [ICD10: I10] [SNOMED: D3-02120,D3-02010,D3-02100,D3-02000,1201005,10725009,59621000,38341003]
		</definition>
		<framework>OSCAREMR</framework>
		<frameworkVersion>03-09-2018</frameworkVersion>
		<notes></notes>
	</heading>
	<indicatorQuery>
		<version>03-09-2018</version>
		<params>
			<!-- 
				Required parameter provider. Value options are: 
					[ an individual providerNo, or provider range ] ie: value="370, 1001" 
					"all" ie: value="all" including null.
					"loggedInProvider" ie:
				Default is "loggedInProvider"
				Use this parameter in the query as ${provider}
				This parameter should be used for fetching patient's assigned to a MRP.
				ie: WHERE demographic.provider_no = ${provider}
			-->
			<parameter id="provider" name="provider_no" value="loggedInProvider" />
			<parameter id="pstatus" name="Patient Status" value="'AC'" />		
			<parameter id="dxcodes" name="Dx Research Codes" value="401,402,403,404,405,I10,D3-02120,D3-02010,D3-02100,D3-02000,1201005,10725009,59621000,38341003" />
			<parameter id="billingcode" name="Billing Diagnosis " value="401,402,403" />
			
			<parameter id="documentation1" name="documentation1" value="'%HTN%'" />
			<parameter id="documentation2" name="documentation2" value="'%HPT%'" />
			<parameter id="documentation3" name="documentation3" value="'%Hypertensive%'" />
			<parameter id="documentation4" name="documentation4" value="'%Hypertension%'" />
		</params>
		<range>
		</range>
		<query>
			<!-- Indicator SQL Query here -->
			SELECT 
				
				IF ( COUNT(fin.patient) > 0,
					SUM( IF( fin.dxexclusion > 0 , 1, 0) ) 
				,0 ) AS "% HTN Coded in PROB or HPH",
				
				IF ( COUNT(fin.patient) > 0,
					SUM( IF( fin.dxexclusion = 0 AND fin.note > 0, 1, 0) ) 
				,0 ) AS "% Documented as text but not coded",
				
				IF ( COUNT(fin.patient) > 0,
					SUM( IF( fin.dxexclusion = 0 AND fin.bill >= 2, 1, 0) ) 
				,0 ) AS "% Dx in 2+ bills",
				
				IF ( COUNT(fin.patient) > 0,
					SUM( IF( fin.dxexclusion = 0 AND (fin.note > 0 OR fin.bill >= 2 ), 1, 0) ) 
				,0 ) AS "% Total"
				
			FROM (
				SELECT 
					d.demographic_no AS patient,
					IFNULL(dxr.dxresearch_no, 0) AS dxexclusion,
					IFNULL(NT.note_id, 0) AS note,
					IFNULL(BILL.id, 0) AS bill
					
				FROM demographic d 
				
				LEFT JOIN (
					SELECT dxresearch_no, demographic_no 
					FROM dxresearch 
					WHERE demographic_no>0 
					AND `status` = "A"
					AND dxresearch_code IN ${dxcodes}
					GROUP BY demographic_no HAVING COUNT(demographic_no) > -1
				) dxr				
				ON ( d.demographic_no = dxr.demographic_no)
				
				-- NOTE
				LEFT JOIN(
					SELECT n.demographic_no, n.note_id
					FROM casemgmt_note n
					INNER JOIN (
						SELECT max(n.note_id) AS note_id, n.demographic_no, n.uuid
						FROM casemgmt_note n, casemgmt_issue c, issue i, casemgmt_issue_notes ino
						WHERE c.issue_id = i.issue_id
						AND ino.id = c.id
						AND n.note_id = ino.note_id
						AND ( i.code = 'Concerns' OR i.code='MedHistory' )

						GROUP BY n.demographic_no, n.uuid HAVING COUNT(n.demographic_no) > -1
					) nn
					ON n.note_id = nn.note_id
					AND n.archived = 0
					AND ( n.note like ${documentation1} OR
                                                        n.note LIKE ${documentation2} OR
                                                        n.note LIKE ${documentation3} OR
                                                        n.note LIKE ${documentation4})

					GROUP BY n.demographic_no
				) NT
				ON (d.demographic_no = NT.demographic_no)
				
				-- WITH 2 or more billing diagnosis codes of 401, 402,403
				LEFT JOIN (
					SELECT COUNT(distinct boc.id) AS id, boc.demographic_no
					FROM billing_on_cheader1 boc, billing_on_item boi
					WHERE boc.id = boi.ch1_id 
					AND boc.status != "D"
					AND boi.status != "D"
					AND boi.dx IN ${billingcode} 
					GROUP BY boc.demographic_no
				) BILL
				ON (d.demographic_no = BILL.demographic_no)		
				
				WHERE d.patient_status = ${pstatus} 
				AND d.provider_no = '${provider}'
				AND d.demographic_no > 0 
				
				GROUP BY d.demographic_no HAVING COUNT(d.demographic_no) > -1
			) fin
		</query>
	</indicatorQuery>
	<drillDownQuery>
		<version>03-09-2018</version>
		<params>
			<parameter id="provider" name="provider_no" value="loggedInProvider" />
			<parameter id="pstatus" name="Patient Status" value="'AC'" />		
			<parameter id="dxcodes" name="Dx Research Codes" value="401,402,403,404,405,I10,D3-02120,D3-02010,D3-02100,D3-02000,1201005,10725009,59621000,38341003" />
			<parameter id="billingcode" name="Billing Diagnosis " value="401,402,403" />
			
			<parameter id="documentation1" name="documentation1" value="'%HTN%'" />
			<parameter id="documentation2" name="documentation2" value="'%HPT%'" />
			<parameter id="documentation3" name="documentation3" value="'%Hypertensive%'" />
			<parameter id="documentation4" name="documentation4" value="'%Hypertension%'" />
		</params>
		<range>
		</range>
		<displayColumns>
			<column id="demographic" name="d.demographic_no" title="Patient Id" primary="true" />
			<column id="name" name="CONCAT( d.last_name, ', ', d.first_name )" title="Patient Name" primary="false" />
			<column id="dob" name="DATE_FORMAT( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth), '%m-%d-%Y' )" title="Patient DOB (mm-dd-yy)" primary="false" />
			<column id="age" name="FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth) ), NOW() ) ) / 365.25 )" title="Patient Age" primary="false" />
			<column id="phonenumber" name="IFNULL( d.phone, '')" title="Patient Phone Number" primary="false" />
			<column id="dxcode" title="HTN Diagnosis Codes Recorded in Disease Registry" name="IFNULL(dxr.dxresearch_code, '')" primary="false" />
<!--
			<column id="htn_concerns" title="HTN Text Recorded in Ongoing Concerns" name="IFNULL(NT_c.note, '')" primary="false" />
			<column id="htn_medicalhistory" title="HTN Text Recorded in Medical History" name="IFNULL(NT_m.note, '')" primary="false" />
			<column id="bill" title="Count of Bills Containing code 401, 402, or 403" name="IFNULL(BILL.id, '')" primary="false" />
-->
		</displayColumns>
		<exportColumns>
			<column id="demographic" name="d.demographic_no" title="Patient Id" primary="true" />
			<column id="firstName" name="d.first_name" title="First Name" primary="false" />
			<column id="lastName" name="d.last_name" title="Last Name" primary="false" />
			<column id="dob" name="DATE_FORMAT( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth), '%m-%d-%Y' )" title="Date of Birth (mm-dd-yy)" primary="false" />
			<column id="age" name="FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth) ), NOW() ) ) / 365.25 )" title="Age" primary="false" />
			<column id="phonenumber" name="IFNULL( d.phone, '')" title="Phone Number" primary="false" />
			<column id="dxcode" title="HTN Diagnosis Codes Recorded in Disease Registry" name="IFNULL(dxr.dxresearch_code, '')" primary="false" />
<!--
                        <column id="htn_concerns" title="HTN Text Recorded in Ongoing Concerns" name="IFNULL(NT_c.note, '')" primary="false" />
                        <column id="htn_medicalhistory" title="HTN Text Recorded in Medical History" name="IFNULL(NT_m.note, '')" primary="false" />
                        <column id="bill" title="Count of Bills Containing code 401, 402, or 403" name="IFNULL(BILL.id, '')" primary="false" />
-->
		</exportColumns>
		<query>
			<!-- Drilldown SQL Query here -->
			SELECT 
			*
			FROM demographic d 
			
			LEFT JOIN (
				SELECT demographic_no, GROUP_CONCAT(dxresearch_code) as dxresearch_code
				FROM dxresearch 
				WHERE demographic_no>0 
				AND `status` = "A"
				AND dxresearch_code IN ${dxcodes}
				GROUP BY demographic_no HAVING COUNT(demographic_no) > -1
			)dxr				
			ON ( d.demographic_no = dxr.demographic_no)
			
			-- NOTE
			LEFT JOIN(
				SELECT n.demographic_no, GROUP_CONCAT(n.note) as note
				FROM casemgmt_note n
				INNER JOIN (
					SELECT max(n.note_id) AS note_id, n.demographic_no, n.uuid
					FROM casemgmt_note n, casemgmt_issue c, issue i, casemgmt_issue_notes ino
					WHERE c.issue_id = i.issue_id
					AND ino.id = c.id
					AND n.note_id = ino.note_id
					AND i.code = 'Concerns'

					GROUP BY n.demographic_no, n.uuid HAVING COUNT(n.demographic_no) > -1
				) nn
				ON n.note_id = nn.note_id
				AND n.archived = 0
				AND ( n.note like ${documentation1} OR
                                                n.note LIKE ${documentation2} OR
                                                n.note LIKE ${documentation3} OR
                                                n.note LIKE ${documentation4})

				GROUP BY n.demographic_no
			) NT_c
			ON (d.demographic_no = NT_c.demographic_no)
			
			LEFT JOIN(
                                SELECT n.demographic_no, GROUP_CONCAT(n.note) as note
                                FROM casemgmt_note n
                                INNER JOIN (
                                        SELECT max(n.note_id) AS note_id, n.demographic_no, n.uuid
                                        FROM casemgmt_note n, casemgmt_issue c, issue i, casemgmt_issue_notes ino
                                        WHERE c.issue_id = i.issue_id
                                        AND ino.id = c.id
                                        AND n.note_id = ino.note_id
                                        AND i.code='MedHistory' 

                                        GROUP BY n.demographic_no, n.uuid HAVING COUNT(n.demographic_no) > -1
                                ) nn
                                ON n.note_id = nn.note_id
                                AND n.archived = 0
                                AND ( n.note like ${documentation1} OR
                                                n.note LIKE ${documentation2} OR
                                                n.note LIKE ${documentation3} OR
                                                n.note LIKE ${documentation4})

                                GROUP BY n.demographic_no
                        ) NT_m
                        ON (d.demographic_no = NT_m.demographic_no)

			-- WITH 2 or more billing diagnosis codes of 401, 402,403
			LEFT JOIN (
				SELECT boc.demographic_no, count(distinct boc.id) as id
				FROM billing_on_cheader1 boc, billing_on_item boi
				WHERE boc.id = boi.ch1_id 
				AND boc.status != "D"
				AND boi.status != "D"
				AND boi.dx IN ${billingcode} 
				GROUP BY boc.demographic_no 
			) BILL
			ON (d.demographic_no = BILL.demographic_no)		
			
			WHERE d.patient_status = ${pstatus} 
			AND d.provider_no = '${provider}'
			AND d.demographic_no > 0 
                        AND dxr.dxresearch_code IS NOT NULL
	
			GROUP BY d.demographic_no HAVING COUNT(d.demographic_no) > -1
		</query>
	</drillDownQuery>
	<shared>false</shared>
	<sharedMetricSetName>OntarioMD HTN Identification</sharedMetricSetName>
	<sharedMetricDataId>Status</sharedMetricDataId>
	<sharedMappings>
		<sharedMapping fromLabel="% HTN Coded in PROB or HPH" toLabel="HTN Coded in PROB or HPH"/>
		<sharedMapping fromLabel="% Documented as text but not coded" toLabel="Consider HTN: Documented as text but not coded"/>
		<sharedMapping fromLabel="% Dx in 2+ bills" toLabel="Consider HTN: Dx in 2+ bills"/>
		<sharedMapping fromLabel="% Total" toLabel="Consider HTN: Total"/>
	</sharedMappings>
</indicatorTemplateXML>
