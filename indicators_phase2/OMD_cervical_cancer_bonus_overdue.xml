<?xml version="1.0" encoding="UTF-8"?>
<indicatorTemplateXML>
	<author>>Trinity Healthcare Technologies for Oscar EMR</author>
	<uid></uid>
	<heading>
		<category>Preventive Health Care</category>
		<subCategory>OMD-Cancer</subCategory>
		<name>OMD-Preventive Care Bonus: Cervical Cancer Screening Test</name>
		<metricSetName>OntarioMD Care Bonus Cervical Cancer Screening</metricSetName>
		<metricLabel>Screening overdue</metricLabel>	
		<definition>Target population: consists of enrolled female patients who are between 21 and 69 years of age, inclusive, as of March 31st of the fiscal year for which the bonus is being claimed. Note: only patients who are sexually active should be used in the calculation for the bonus Service Period: by 42 months prior to March 31st of the fiscal year for which the bonus is being claimed. Service Codes reported: G365A, L713A, L643A, E430A, E431A, Q678A, tracking code Q011A and exclusion code Q140A</definition>
		<framework>Based on and adapted from HQO's PCPM: Priority Measures for System and Practice Levels (Oct 2015)</framework>
		<frameworkVersion>02-08-2018</frameworkVersion>
		<notes></notes>
	</heading>
	<indicatorQuery>
		<version>02-08-2018</version>
		<params>
			<!-- 
				Required parameter provider. Value options are: 
					[ an individual providerNo, or provider range ] ie: value="370, 1001" 
					"all" ie: value="all" including null.
					"loggedInProvider" ie:
				Default is "loggedInProvider"
				Use this parameter in the query as ${provider}
				This parameter should be used for fetching patient's assigned to a MRP.
				ie: WHERE demographic.provider_no = ${provider}
			-->
			<parameter id="provider" name="provider_no" value="loggedInProvider" />
			<parameter id="exclusionCode" name="billing_exclusion_code" value="'Q140A'" />
			<parameter id="serviceCode" name="billing_service_code" value="'Q011A'" />
			<parameter id="dxexclusion" name="exclusion_dx_code" value="V4577,683,684,685,686,687,688,689" />
			<parameter id="prevention" name="prevention_code" value="'PAP'" />
			<parameter id="pstatus" name="Patient Status" value="'AC'" />
			<parameter id="loincpap" name="PAP LOINC" value="47527-7,49050-8,10524-7" />
			<parameter id="rosterstatus" name="Patient Roster Status" value="'RO'" />	
			<parameter id="gender" name="Patient Gender" value="'F'" />		
			<parameter id="documentation1" name="documentation1" value="'%hysterectomy%'" />
			<parameter id="documentation2" name="documentation2" value="'%hysterosal%'" />
			<parameter id="documentation3" name="documentation3" value="'%Cervical Ca%'" />
			<parameter id="documentation4" name="documentation4" value="'%TVH%'" />
			<parameter id="documentation5" name="documentation5" value="'%TABH%'" />
			<parameter id="documentation6" name="documentation6" value="'%TAH%'" />
		</params>
		<range>
			<upperLimit id="age" label="Max Age" name="Age" value="69" />
			<lowerLimit id="age" label="Min Age" name="Age" value="21" />
			
			<upperLimit id="date" label="March 31" name="Date Upper" value="STR_TO_DATE( CONCAT( '31,', '03,', YEAR(NOW()) ),'%d,%m,%Y')" />
			<lowerLimit id="date" label="Months History" name="Date Lower" value="42" />
			<lowerLimit id="date42" label="Months 42" name="Date 42" value="DATE_SUB( NOW(), INTERVAL 42 MONTH )" />
			<lowerLimit id="date24" label="Months 24" name="Date 24" value="DATE_SUB( NOW(), INTERVAL 24 MONTH )" />
		</range>
		<query>
			<!-- Indicator SQL Query here -->
			SELECT 

				IF ( COUNT(fin.patient) > 0,
					SUM( IF( (fin.papm > 0 or fin.pap > 0 OR fin.bill > 0 ) AND fin.bexclusion = 0 AND fin.pap_i = 0 AND fin.dxexclusion = 0, 1, 0) ) 
				,0 ) AS "% Eligible",
			
				IF ( COUNT(fin.patient) > 0,
					 SUM( IF( fin.papm = 0 AND fin.pap = 0 AND fin.bill = 0 AND fin.bexclusion = 0 AND fin.pap_i = 0 AND fin.dxexclusion = 0 , 1, 0) )
				,0 ) AS "% Ineligible"
			
			FROM (
			
				SELECT 
			
					d.demographic_no AS patient,
					IFNULL(PAP.id, 0) AS pap, 
					IFNULL(EXCL.id, 0) AS bexclusion,
					IFNULL(BILL.id, 0) AS bill,
					IFNULL(dxr.dxresearch_no, 0) AS dxexclusion,
					IFNULL(PAP_i.id, 0) AS pap_i,
					IFNULL(PAPM.id, 0) as papm
			
				FROM demographic d
				LEFT JOIN (
					SELECT dxresearch_no, demographic_no 
					FROM dxresearch 
					WHERE demographic_no>0 
					AND status = 'A'
					AND coding_system = 'icd9'
					AND dxresearch_code IN ${dxexclusion} 
					GROUP BY demographic_no HAVING COUNT(demographic_no) > -1
				)dxr				
				ON ( d.demographic_no = dxr.demographic_no) 
				
				-- Look for possible PAP results in measurements.
				LEFT JOIN (
                                        SELECT m2.demographicNo, m2.id
                                        FROM measurements m2
                                        JOIN measurementsExt me
                                        ON m2.id = me.measurement_id
                                        JOIN measurementMap mm
                                        ON me.val = mm.`name`
                                        WHERE me.keyval = "name"
                                        AND mm.loinc_code IN ${loincpap}
                                        AND DATE(m2.dateObserved) BETWEEN DATE_SUB( ${upperLimit.date}, INTERVAL ${lowerLimit.date} MONTH ) AND ${upperLimit.date}

                                        GROUP BY m2.demographicNo

                                ) PAPM
                                ON (d.demographic_no = PAPM.demographicNo)

			
				-- GET ALL CURRENT PAP ENTRIES FROM PREVENTIONS
				LEFT JOIN (
                                        SELECT p.demographic_no, p.id, pe.val, p.prevention_date
                                        FROM preventions p
                                        JOIN preventionsExt pe
                                        ON p.id = pe.prevention_id
                                        RIGHT JOIN (
                                                SELECT p.demographic_no, MAX(p.prevention_date) AS prevention_date
                                                FROM preventions p
                                                JOIN preventionsExt pe
                                                ON p.id = pe.prevention_id
                                                WHERE p.prevention_type = ${prevention}
                                                AND p.deleted = 0
                                                AND p.refused = 0
                                                AND pe.keyval = "result"
                                                AND pe.val != "pending"
	                                        AND DATE(p.prevention_date) BETWEEN DATE_SUB( ${upperLimit.date}, INTERVAL ${lowerLimit.date} MONTH ) AND ${upperLimit.date}

                                                GROUP BY demographic_no
                                        ) p2
                                        ON p.prevention_date = p2.prevention_date
                                        AND p.demographic_no = p2.demographic_no
                                        AND p.prevention_type = ${prevention}
					AND p.deleted = 0
					AND p.refused = 0
                                        WHERE pe.keyval = "result"
                                        AND pe.val != "pending"
                                        ORDER BY p.id DESC
                                ) PAP
                                ON (d.demographic_no = PAP.demographic_no)

				-- TRACKING/EXCLUSION CODE USED.
				LEFT JOIN (
                                        SELECT boc.id, boc.demographic_no, boi.service_date
                                        FROM billing_on_cheader1 boc, billing_on_item boi
                                        WHERE boc.id = boi.ch1_id
                                        AND boc.status != 'D'
                                        AND boi.status != 'D'
                                        AND boi.service_code = ${exclusionCode}
                                        GROUP BY boc.demographic_no
                                ) EXCL
                                ON d.demographic_no = EXCL.demographic_no

				LEFT JOIN (
                                        SELECT boc.id, boc.demographic_no, boi.service_date
                                        FROM billing_on_cheader1 boc, billing_on_item boi
                                        WHERE boc.id = boi.ch1_id
                                        AND boc.status != 'D'
                                        AND boi.status != 'D'
                                        AND boi.service_code = ${serviceCode}
					AND DATE(boi.service_date) BETWEEN DATE_SUB( ${upperLimit.date}, INTERVAL ${lowerLimit.date} MONTH ) AND ${upperLimit.date}
                                        GROUP BY boc.demographic_no
                                ) BILL
                                ON d.demographic_no = BILL.demographic_no

				-- Notes replaced with ineligible
                                -- Use Ineligible in 24month searching to replace note searching
                                LEFT JOIN (

                                        SELECT demographic_no, id
                                        FROM preventions
                                        WHERE prevention_type = ${prevention}
                                        AND deleted = 0
                                        AND refused = 2
					AND DATE(prevention_date) BETWEEN DATE_SUB( ${upperLimit.date}, INTERVAL ${lowerLimit.date} MONTH ) AND ${upperLimit.date}
                                        GROUP BY demographic_no

                                ) PAP_i
                                ON (d.demographic_no = PAP_i.demographic_no)

				WHERE d.patient_status = ${pstatus}
				AND d.provider_no = '${provider}'
				AND d.sex = ${gender}
				AND d.roster_status = ${rosterstatus}
				AND d.demographic_no > 0 
				AND FLOOR( ABS( DATEDIFF( STR_TO_DATE( CONCAT(d.year_of_birth,",",d.month_of_birth,",",d.date_of_birth), '%Y,%m,%d' ), ${upperLimit.date} ) / 365.25 ) ) 
				BETWEEN ${lowerLimit.age} AND ${upperLimit.age}
				GROUP BY d.demographic_no HAVING COUNT(d.demographic_no) > -1
			) fin
		</query>
	</indicatorQuery>
	<drillDownQuery>
		<version>02-08-2018</version>
		<params>
			<parameter id="provider" name="provider_no" value="loggedInProvider" />
			<parameter id="exclusionCode" name="billing_exclusion_code" value="'Q140A'" />
			<parameter id="dxexclusion" name="exclusion_dx_code" value="V4577,683,684,685,686,687,688,689" />
			<parameter id="prevention" name="prevention_code" value="'PAP'" />
			<parameter id="pstatus" name="Patient Status" value="'AC'" />
			<parameter id="loincpap" name="PAP LOINC" value="47527-7,49050-8,10524-7" />
			<parameter id="rosterstatus" name="Patient Roster Status" value="'RO'" />	
			<parameter id="gender" name="Patient Gender" value="'F'" />
			<parameter id="documentation1" name="documentation1" value="'%hysterectomy%'" />
			<parameter id="documentation2" name="documentation2" value="'%hysterosal%'" />
			<parameter id="documentation3" name="documentation3" value="'%Cervical Ca%'" />
			<parameter id="documentation4" name="documentation4" value="'%TVH%'" />
			<parameter id="documentation5" name="documentation5" value="'%TABH%'" />
			<parameter id="documentation6" name="documentation6" value="'%TAH%'" />
			
			<parameter id="serviceCode" name="billing_service_codes" value="G365A,L713A,L643A,E430A,E431A,Q678A" />
			<parameter id="trackingCode" name="billing_tracking_codes" value="'Q011A'" />
			<parameter id="graceperiod" name="Grace Period Months" value="6" />			
		</params>
		<range>
			<upperLimit id="age" label="Max Age" name="Age" value="69" />
			<lowerLimit id="age" label="Min Age" name="Age" value="21" />
			
			<upperLimit id="date" label="March 31" name="Date Upper" value="STR_TO_DATE( CONCAT( '31,', '03,', YEAR(NOW()) ),'%d,%m,%Y')" />
			<lowerLimit id="date" label="Months History" name="Date Lower" value="42" />
			<lowerLimit id="date42" label="Months 42" name="Date 42" value="DATE_SUB( NOW(), INTERVAL 42 MONTH )" />
			<lowerLimit id="date24" label="Months 24" name="Date 24" value="DATE_SUB( NOW(), INTERVAL 24 MONTH )" />
		</range>
		<displayColumns>
			<column id="demographic" name="d.demographic_no" title="Patient Id" primary="true" />
			<column id="name" name="CONCAT( d.last_name, ', ', d.first_name )" title="Patient Name" primary="false" />
			<column id="dob" name="DATE_FORMAT( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth), '%m-%d-%Y' )" title="Patient DOB (mm-dd-yyyy)" primary="false" />
			<column id="age" name="FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth) ), NOW() ) ) / 365.25 )" title="Patient Age" primary="false" />
			<column id="phonenumber" name="IFNULL( d.phone, '')" title="Patient Phone Number" primary="false" />

			<column id="papstatus" name="IFNULL(PAP0.refused, '')" title="PAP Tracking Status" primary="false" />
			<column id="pap" name="IFNULL(PAP0.val, '')" title="PAP Result" primary="false" />
			<column id="lastdate" name="IFNULL( DATE_FORMAT(PAP0.prevention_date, '%m-%d-%Y' ), '')" title="Latest PAP Test Date (mm-dd-yyyy)" primary="false" />
			<column id="trackingservice" name="IFNULL(DATE_FORMAT(BILL0.service_date, '%m-%d-%Y' ), '')" title="Latest Q011A Date (mm-dd-yyyy)" primary="false" />
<!--			<column id="march31" name="IFNULL(ROUND( DATEDIFF( ${upperLimit.date}, DATE(PAP.prevention_date)) / 365.25 * 12, 1 ), 0)" title="March 31" primary="false" />
			<column id="sept30" name="IFNULL(ROUND( DATEDIFF( DATE_ADD( ${upperLimit.date}, INTERVAL ${graceperiod} MONTH  ), DATE(PAP.prevention_date)) / 365.25 * 12, 1 ), 0)" title="Sept 30" primary="false" />
			<column id="today" name="IFNULL(ROUND( DATEDIFF( NOW(), DATE(PAP.prevention_date)) / 365.25 * 12, 1 ), 0)" title="Today" primary="false" />
			<column id="status" name="CASE 
					WHEN PAP.refused IS NULL
					THEN 'No Test'		
					WHEN IFNULL(PAP.refused, -1) = 0 
						AND ( DATE(PAP.prevention_date) BETWEEN DATE_SUB( ${upperLimit.date}, INTERVAL ${lowerLimit.date} MONTH ) AND ${upperLimit.date} )
					THEN 'Eligible'		
					WHEN IFNULL(PAP.refused, -1) = 0 
						AND ( DATE(PAP.prevention_date) > ${upperLimit.date} )
					THEN 'Done'		
					WHEN IFNULL(PAP.refused, -1) = 1
					THEN 'Refused'		
					WHEN IFNULL(PAP.refused, -1) = 2 
						OR IFNULL(dxr.dxresearch_no, -1) > 0 
						OR IFNULL(TRACK.service_code, -1) LIKE ${exclusionCode}
					THEN 'Ineligible'		
					ELSE 'Overdue'
				END" title="Status" primary="false" />
			<column id="trackingcode" name="IFNULL(TRACK.service_code,0)" title="Tracking / Exclusion Code" primary="false" />
			<column id="servicecode" name="IFNULL(BILL.service_code,0)" title="Service Code" primary="false" />
-->		
		</displayColumns>
		<query>
			<!-- Drilldown SQL Query here -->	
			SELECT 
				d.demographic_no AS 'ID', 
				CONCAT( d.last_name, ", ",d.first_name ) AS 'Name',
				DATE_FORMAT( CONCAT(d.year_of_birth,"-",d.month_of_birth,"-",d.date_of_birth), '%m-%d-%Y' ) AS 'Birth Date (mm-dd-yyyy)',
				FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,"-",d.month_of_birth,"-",d.date_of_birth) ), NOW() ) ) / 365.25 ) AS 'Age'
				
			FROM demographic d
			LEFT JOIN (
				SELECT dxresearch_no, demographic_no
                                FROM dxresearch
                                WHERE demographic_no>0
                                AND status = 'A'
				AND coding_system = 'icd9'
                                AND dxresearch_code IN ${dxexclusion}
                                GROUP BY demographic_no HAVING COUNT(demographic_no) > -1
                        )dxr
                        ON ( d.demographic_no = dxr.demographic_no)

			
			-- Look for possible PAP results in measurements.
			LEFT JOIN (
                                SELECT m2.demographicNo, m2.id
                                FROM measurements m2
                                JOIN measurementsExt me
                                ON m2.id = me.measurement_id
                                JOIN measurementMap mm
                                ON me.val = mm.`name`
                                WHERE me.keyval = "name"
                                AND mm.loinc_code IN ${loincpap}
				AND DATE(m2.dateObserved) BETWEEN DATE_SUB( ${upperLimit.date}, INTERVAL ${lowerLimit.date} MONTH ) AND ${upperLimit.date}
                                GROUP BY m2.demographicNo

                        ) PAPM
                        ON (d.demographic_no = PAPM.demographicNo)

			-- GET ALL PAP ENTRIES FROM PREVENTIONS
			LEFT JOIN (
                                SELECT p.demographic_no, p.id, pe.val, p.prevention_date
                                FROM preventions p
                                JOIN preventionsExt pe
                                ON p.id = pe.prevention_id
                                RIGHT JOIN (
                                        SELECT p.demographic_no, MAX(p.prevention_date) AS prevention_date
                                        FROM preventions p
                                        JOIN preventionsExt pe
                                        ON p.id = pe.prevention_id
                                        WHERE p.prevention_type = ${prevention}
                                        AND p.deleted = 0
                                        AND p.refused = 0
                                        AND pe.keyval = "result"
                                        AND pe.val != "pending"
					AND DATE(p.prevention_date) BETWEEN DATE_SUB( ${upperLimit.date}, INTERVAL ${lowerLimit.date} MONTH ) AND ${upperLimit.date}
                                        GROUP BY demographic_no
                                ) p2
                                ON p.prevention_date = p2.prevention_date
                                AND p.demographic_no = p2.demographic_no
                                AND p.prevention_type = ${prevention}
				AND p.deleted = 0
				AND p.refused = 0
                                WHERE pe.keyval = "result"
                                AND pe.val != "pending"
                                ORDER BY p.id DESC
                        ) PAP
                        ON (d.demographic_no = PAP.demographic_no)

			LEFT JOIN (
                                SELECT p.demographic_no, p.id, pe.val, p.prevention_date, IF(p.refused = 1, "Refused", IF(p.refused = 2, "Ineligible", "Complete") ) AS refused
                                FROM preventions p
                                JOIN preventionsExt pe
                                ON p.id = pe.prevention_id
                                RIGHT JOIN (
                                        SELECT demographic_no, MAX(prevention_date) AS prevention_date
                                        FROM preventions
                                        WHERE prevention_type = ${prevention}
                                        AND deleted = 0
                                        GROUP BY demographic_no
                                ) p2
                                ON p.prevention_date = p2.prevention_date
                                AND p.demographic_no = p2.demographic_no
                                AND p.prevention_type = ${prevention}
				AND p.deleted = 0
                                WHERE pe.keyval = "result"
                                ORDER BY p.id DESC

                        ) PAP0
                        ON (d.demographic_no = PAP0.demographic_no )


			-- TRACKING/EXCLUSION CODE USED.
			LEFT JOIN (
                                SELECT boc.id, boc.demographic_no, boi.service_date
                                FROM billing_on_cheader1 boc, billing_on_item boi
                                WHERE boc.id = boi.ch1_id
                                AND boc.status != 'D'
                                AND boi.status != 'D'
                                AND boi.service_code = ${exclusionCode}
                                GROUP BY boc.demographic_no
                        ) EXCL
                        ON d.demographic_no = EXCL.demographic_no

			-- SERVICE CODE USED
			LEFT JOIN (
                                SELECT boc.id, boc.demographic_no, boi.service_date
                                FROM billing_on_cheader1 boc, billing_on_item boi
                                WHERE boc.id = boi.ch1_id
                                AND boc.status != 'D'
                                AND boi.status != 'D'
                                AND boi.service_code = ${trackingCode}
				AND DATE(boi.service_date) BETWEEN DATE_SUB( ${upperLimit.date}, INTERVAL ${lowerLimit.date} MONTH ) AND ${upperLimit.date}
                                GROUP BY boc.demographic_no
                        ) BILL
                        ON d.demographic_no = BILL.demographic_no

			LEFT JOIN (
                                SELECT boc.id, boc.demographic_no, boi.service_date
                                FROM billing_on_cheader1 boc, billing_on_item boi
                                WHERE boc.id = boi.ch1_id
                                AND boc.status != 'D'
                                AND boi.status != 'D'
                                AND boi.service_code = ${trackingCode}
                                GROUP BY boc.demographic_no
                        ) BILL0
                        ON d.demographic_no = BILL0.demographic_no

			
			LEFT JOIN (

                                SELECT demographic_no, id
                                FROM preventions
                                WHERE prevention_type = ${prevention}
                                AND deleted = 0
                                AND refused = 2
				AND DATE(prevention_date) BETWEEN DATE_SUB( ${upperLimit.date}, INTERVAL ${lowerLimit.date} MONTH ) AND ${upperLimit.date}
                                GROUP BY demographic_no

                        ) PAP_i
                        ON (d.demographic_no = PAP_i.demographic_no)
	
		
			WHERE d.patient_status = ${pstatus}
			AND d.provider_no = '${provider}'
			AND d.sex = ${gender}
			AND d.roster_status = ${rosterstatus}
			AND d.demographic_no > 0 
			AND FLOOR( ABS( DATEDIFF( STR_TO_DATE( CONCAT(d.year_of_birth,",",d.month_of_birth,",",d.date_of_birth), '%Y,%m,%d' ), ${upperLimit.date} ) / 365.25 ) ) BETWEEN ${lowerLimit.age} AND ${upperLimit.age}
                        AND PAPM.id IS NULL AND PAP.id IS NULL AND BILL.id IS NULL AND EXCL.id IS NULL AND dxr.dxresearch_no IS NULL AND PAP_i.id IS NULL

			GROUP BY d.demographic_no HAVING COUNT(d.demographic_no) > -1
		</query>
	</drillDownQuery>
	<shared>false</shared>
	<sharedMetricSetName>OntarioMD Care Bonus Cervical Cancer Screening</sharedMetricSetName>
	<sharedMetricDataId>Status</sharedMetricDataId>
	<sharedMappings>
		<sharedMapping fromLabel="% Eligible" toLabel="Screening up to date"/>
		<sharedMapping fromLabel="% Ineligible" toLabel="Screening overdue"/>
	</sharedMappings>
</indicatorTemplateXML>
