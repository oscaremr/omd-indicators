<?xml version="1.0" encoding="UTF-8"?>
<indicatorTemplateXML>
	<author>Colcamex Resources Inc. for Oscar EMR</author>
	<uid></uid>
	<heading>
		<category>Preventive Health Care</category>
		<subCategory>OMD-Cancer</subCategory>
		<name>OMD-Patient Care: Breast Cancer Screening</name>
		<metricSetName>OntarioMD Breast Cancer Screening</metricSetName>
		<metricLabel>Screening overdue</metricLabel>
		<definition>% of female patients aged 50 to 74 who had a mammogram within the past two years:
		- Number of individuals in the denominator who had a mammogram within the past 24 months.
		- excluding patients with a diagnostic code V4571 for Mastectomy
		- patients with a null or “pending” prevention result are overdue.
		</definition>
		<framework>Based on and adapted from CIHI’s 2012 Indicator Technical Specifications (Nov 2012)</framework>
		<frameworkVersion>01-30-2018</frameworkVersion>
		<notes></notes>
	</heading>
	<indicatorQuery>
		<version>01-30-2018</version>
		<params>
			<!-- 
				Required parameter provider. Value options are: 
					[ an individual providerNo, or provider range ] ie: value="370, 1001" 
					"all" ie: value="all" including null.
					"loggedInProvider" ie:
				Default is "loggedInProvider"
				Use this parameter in the query as ${provider}
				This parameter should be used for fetching patient's assigned to a MRP.
				ie: WHERE demographic.provider_no = ${provider}
			-->
			<parameter id="provider" name="provider_no" value="loggedInProvider" />
			<parameter id="pstatus" name="Patient Status" value="'AC'" />
			<parameter id="prevention" name="MAM" value="'MAM'" />
			<parameter id="gender" name="GenderFemale" value="'F'" />
			<parameter id="dxexclude" name="Exclude Dx" value="V4571, 174" />		
			<parameter id="servicecode1" name="Service Code" value="'Q131A'" />
			<parameter id="servicecode2" name="Service Code" value="'Q141A'" />
			<parameter id="documentation1" name="documentation1" value="'%mastectomy%'" />
			<parameter id="documentation2" name="documentation2" value="'%cancer breast%'" />
			<parameter id="documentation3" name="documentation3" value="'%breast ca%'" />
			<parameter id="documentation4" name="documentation4" value="'%ca breast%'" />	
		</params>
		<range>
			<upperLimit id="age" label="Max Age" name="Age" value="74" />
			<lowerLimit id="age" label="Min Age" name="Age" value="50" />
			
			<upperLimit id="date" label="Current Date" name="CurrentDate" value="NOW()" />			
			<lowerLimit id="date" label="24 Months" name="Date24" value="DATE_SUB( NOW(), INTERVAL 24 MONTH )" />
		</range>
		<query>
			<!-- Indicator SQL Query here -->
			SELECT 

			    IF ( COUNT(fin.patient) > 0,
                                        SUM( IF( ( fin.mam > 0 OR fin.billQ131A > 0) AND fin.mam_i IS NULL AND fin.dx IS NULL AND fin.billQ141A IS NULL , 1, 0 ) )
                                ,0 ) AS "% Mammogram Done",

                                IF ( COUNT(fin.patient) > 0,
                                        SUM( IF( fin.mam IS NULL AND fin.billQ131A IS NULL AND fin.billQ141A IS NULL AND fin.mam_i IS NULL AND fin.dx IS NULL, 1, 0 ) )
                                ,0 ) AS "% Not Done",

                                IF ( COUNT(fin.patient) > 0,
                                        SUM( IF( fin.mam_i > 0 OR fin.dx > 0 OR fin.billQ141A > 0, 1, 0) )
                                ,0 ) AS "% Excluded"

			FROM (
				SELECT 
					d.demographic_no AS 'patient', 
					MAM.id AS 'mam',
					MAM.val AS 'status',
					BILL1.id AS billQ131A, 
					BILL2.id AS billQ141A,
					MAM_i.id AS 'mam_i',
					dxr.dxresearch_no AS dx 
				FROM demographic d
				LEFT JOIN ( 
					SELECT dxresearch_no, demographic_no
                                        FROM dxresearch
                                        WHERE demographic_no>0
					AND status = 'A'
                                        AND dxresearch_code IN ${dxexclude}
                                        GROUP BY demographic_no HAVING COUNT(demographic_no) > -1
				)dxr 
				ON ( d.demographic_no = dxr.demographic_no) 
			
				-- GET ALL MAMMOGRAM ENTRIES FROM PREVENTIONS
				LEFT JOIN ( 
					SELECT p.demographic_no, p.id, pe.val, p.refused 
					FROM preventions p		
					JOIN preventionsExt pe
					ON pe.prevention_id = p.id 
					WHERE p.prevention_type = ${prevention}
					AND p.deleted = 0
					AND p.refused = 0
					AND pe.keyval = "result" 
					AND pe.val != "pending"
					AND DATE(p.prevention_date) >= ${lowerLimit.date}
					GROUP BY p.demographic_no HAVING COUNT(p.demographic_no) > -1 
				) MAM
				ON (d.demographic_no = MAM.demographic_no)
				
				-- WITHOUT a billing for Q131A in the last 24 months
				LEFT JOIN (
					SELECT boc.id, boc.demographic_no, boc.billing_date
					FROM billing_on_cheader1 boc, billing_on_item boi
					WHERE boc.id = boi.ch1_id 
					AND boc.status != 'D'
                                	AND boi.status != 'D'
					AND boi.service_code =  ${servicecode1}
					AND DATE(boi.service_date) >= ${lowerLimit.date} 
					GROUP BY boc.demographic_no 
				) BILL1
				ON (d.demographic_no = BILL1.demographic_no)
				
				-- WITHOUT a service code of Q141A billed
				LEFT JOIN (
					SELECT boc.id, boc.demographic_no, boc.billing_date
					FROM billing_on_cheader1 boc, billing_on_item boi
					WHERE boc.id = boi.ch1_id 
					AND boc.status != 'D'
                                	AND boi.status != 'D'
					AND boi.service_code = ${servicecode2}
					GROUP BY boc.demographic_no 
				) BILL2
				ON (d.demographic_no = BILL2.demographic_no)
				
				 -- Use Ineligible in 24month searching to replace note searching
                        	LEFT JOIN (

                                	SELECT demographic_no, id
                                	FROM preventions
                                	WHERE prevention_type = ${prevention}
                                	AND deleted = 0
                                	AND refused = 2
                                	AND prevention_date >= ${lowerLimit.date}
                                	GROUP BY demographic_no

                        	) MAM_i
                        	ON (d.demographic_no = MAM_i.demographic_no)

				WHERE d.patient_status = ${pstatus}
				AND d.provider_no = '${provider}'
				AND d.sex = ${gender}
				AND d.demographic_no > 0 
				AND FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,"-",d.month_of_birth,"-",d.date_of_birth) ), ${upperLimit.date} ) / 365.25 ) ) 
					BETWEEN ${lowerLimit.age} AND ${upperLimit.age}
				GROUP BY d.demographic_no HAVING COUNT(d.demographic_no) > -1
			) fin
		</query>
	</indicatorQuery>
	<drillDownQuery>
		<version>01-30-2018</version>
		<params>
			<parameter id="provider" name="provider_no" value="loggedInProvider" />
			<parameter id="pstatus" name="Patient Status" value="'AC'" />
			<parameter id="prevention" name="MAM" value="'MAM'" />
			<parameter id="gender" name="GenderFemale" value="'F'" />
			<parameter id="dxexclude" name="Exclude Dx" value="V4571, 174" />		
			<parameter id="servicecode1" name="Service Code" value="'Q131A'" />
			<parameter id="servicecode2" name="Service Code" value="'Q141A'" />
			<parameter id="documentation1" name="documentation1" value="'%mastectomy%'" />
			<parameter id="documentation2" name="documentation2" value="'%cancer breast%'" />
			<parameter id="documentation3" name="documentation3" value="'%breast ca%'" />
			<parameter id="documentation4" name="documentation4" value="'%ca breast recorded%'" />
		</params>
		<range>
			<upperLimit id="age" label="Max Age" name="Age" value="74" />
			<lowerLimit id="age" label="Min Age" name="Age" value="50" />
			
			<upperLimit id="date" label="Current Date" name="CurrentDate" value="NOW()" />			
			<lowerLimit id="date" label="24 Months" name="Date24" value="DATE_SUB( NOW(), INTERVAL 24 MONTH )" />
		</range>
		<displayColumns>
			<column id="demographic" name="d.demographic_no" title="Patient Id" primary="true" />
			<column id="name" name="CONCAT( d.last_name, ', ', d.first_name )" title="Patient Name" primary="false" />
			<column id="dob" name="DATE_FORMAT( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth), '%m-%d-%Y' )" title="Patient DOB (mm-dd-yyyy)" primary="false" />
			<column id="age" name="FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth) ), NOW() ) ) / 365.25 )" title="Patient Age" primary="false" />	
			<column id="phonenumber" name="IFNULL( d.phone, '')" title="Patient Phone Number" primary="false" />
			<column id="rosterstatus" name="IFNULL( d.roster_status, '')" title="Patient Roster Status" primary="false" />
			<column id="rosterdate" name="IFNULL( DATE_FORMAT(d.roster_date, '%m-%d-%Y' ), '')" title="Rostered Date (mm-dd-yyyy)" primary="false" />
                        <column id="trackingstatus" name="IFNULL( MAM_t.refused, '')" title="Mammogram Tracking Status" primary="false" />
                        <column id="mamstatus" name="IFNULL(MAM_t.val, '')" title="Mammogram Result" primary="false" />
                        <column id="mamdate" name="IFNULL( DATE_FORMAT(MAM_t.prevention_date, '%m-%d-%Y' ), '')" title="Latest Mammogram Test Date (mm-dd-yyyy)" primary="false" />

                        <column id="billQ131ADate" name="IFNULL( DATE_FORMAT(BILL1_0.billing_date, '%m-%d-%Y' ), '')" title="Latest Q131A Date (mm-dd-yyyy)" primary="false" />
                        <column id="lastappdate" name="IFNULL( DATE_FORMAT(app1.appointment_date, '%m-%d-%Y' ), '')" title="Last Seen Date (mm-dd-yyyy)" primary="false" />
                        <column id="nextappdate" name="IFNULL( DATE_FORMAT(app2.appointment_date, '%m-%d-%Y' ), '')" title="Next Appointment Date (mm-dd-yyyy)" primary="false" />
<!--                        <column id="billQ141ADate" name="IFNULL( DATE_FORMAT(BILL2.billing_date, '%m-%d-%Y' ), '')" title="Latest Q141A Date (mm-dd-yyyy)" primary="false" />
-->
		</displayColumns>
		<exportColumns>
			<column id="demographic" name="d.demographic_no" title="Patient Id" primary="true" />
			<column id="firstName" name="d.first_name" title="First Name" primary="false" />
			<column id="lastName" name="d.last_name" title="Last Name" primary="false" />
			<column id="dob" name="DATE_FORMAT( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth), '%m-%d-%Y' )" title="Patient DOB (mm-dd-yyyy)" primary="false" />
			<column id="age" name="FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth) ), NOW() ) ) / 365.25 )" title="Patient Age" primary="false" />	
			<column id="phonenumber" name="IFNULL( d.phone, '')" title="Patient Phone Number" primary="false" />
			<column id="rosterstatus" name="IFNULL( d.roster_status, '')" title="Patient Roster Status" primary="false" />
			<column id="rosterdate" name="IFNULL( DATE_FORMAT(d.roster_date, '%m-%d-%Y' ), '')" title="Rostered Date (mm-dd-yyyy)" primary="false" />
                        <column id="trackingstatus" name="IFNULL( MAM_t.refused, '')" title="Mammogram Tracking Status" primary="false" />
                        <column id="mamstatus" name="IFNULL(MAM_t.val, '')" title="Mammogram Result" primary="false" />
                        <column id="mamdate" name="IFNULL( DATE_FORMAT(MAM_t.prevention_date, '%m-%d-%Y' ), '')" title="Latest Mammogram Test Date (mm-dd-yyyy)" primary="false" />
                        <column id="billQ131ADate" name="IFNULL( DATE_FORMAT(BILL1_0.billing_date, '%m-%d-%Y' ), '')" title="Latest Q131A Date (mm-dd-yyyy)" primary="false" />
                        <column id="lastappdate" name="IFNULL( DATE_FORMAT(app1.appointment_date, '%m-%d-%Y' ), '')" title="Last Seen Date  (mm-dd-yyyy)" primary="false" />
                        <column id="nextappdate" name="IFNULL( DATE_FORMAT(app2.appointment_date, '%m-%d-%Y' ), '')" title="Next Appointment Date (mm-dd-yyyy)" primary="false" />
  <!--                      <column id="billQ141ADate" name="IFNULL( DATE_FORMAT(BILL2.billing_date, '%m-%d-%Y' ), '')" title="Latest Q141A Date (mm-dd-yyyy)" primary="false" />
-->		</exportColumns>
		<query>
			<!-- Drilldown SQL Query here -->
			SELECT 
			*
			FROM demographic d
			LEFT JOIN ( 
				SELECT dxresearch_no, demographic_no
				FROM dxresearch
				WHERE demographic_no>0
				AND status = 'A'
				AND dxresearch_code IN ${dxexclude}
				GROUP BY demographic_no HAVING COUNT(demographic_no) > -1
			
			)dxr 
			ON ( d.demographic_no = dxr.demographic_no) 
		
			-- GET Mam done in 24months
			LEFT JOIN ( 
		
				SELECT p.demographic_no, p.id, pe.val
				FROM preventions p	
				JOIN preventionsExt pe
				ON p.id = pe.prevention_id
				WHERE p.prevention_type = ${prevention}
				AND p.deleted = 0 AND p.refused = 0
				AND pe.keyval = "result"
				AND pe.val != "pending"
				AND p.prevention_date >= ${lowerLimit.date}
				GROUP BY demographic_no
		
			) MAM
			ON (d.demographic_no = MAM.demographic_no)
			
			-- Use Ineligible in 24month searching to replace note searching
			LEFT JOIN (

                                SELECT demographic_no, id
                                FROM preventions 
                                WHERE prevention_type = ${prevention}
                                AND deleted = 0
				AND refused = 2
				AND prevention_date >= ${lowerLimit.date}
                                GROUP BY demographic_no

                        ) MAM_i
                        ON (d.demographic_no = MAM_i.demographic_no)


			-- latest tracking status
			 LEFT JOIN (

                                SELECT p.demographic_no, p.id, pe.val, p.prevention_date, IF(p.refused = 1, "Refused", IF(p.refused = 2, "Ineligible", "Complete") ) AS refused
                                FROM preventions p
				JOIN preventionsExt pe
                                ON p.id = pe.prevention_id
				RIGHT JOIN (
					SELECT demographic_no, MAX(prevention_date) AS prevention_date
					FROM preventions
					WHERE prevention_type = ${prevention}
					AND deleted = 0
					GROUP BY demographic_no
				) p2
				ON p.prevention_date = p2.prevention_date
				AND p.demographic_no = p2.demographic_no
				AND p.prevention_type = ${prevention}	
                                AND p.deleted = 0
				WHERE pe.keyval = "result"
				ORDER BY p.id DESC
			) MAM_t
                        ON (d.demographic_no = MAM_t.demographic_no)


			-- Last Seen Date
			LEFT JOIN (
				SELECT a.demographic_no, max(a.appointment_date) as appointment_date 
				FROM appointment a, demographic d 
				WHERE a.demographic_no = d.demographic_no
				AND d.provider_no = '${provider}'
				AND ${upperLimit.date} > DATE(a.appointment_date)
				AND a.status != 'D' AND a.status != 'C' AND a.status != 'N'
                                AND a.demographic_no > 0
                                GROUP BY a.demographic_no
			) app1
			ON (d.demographic_no = app1.demographic_no)

			-- Next Appointment Date
			LEFT JOIN (
				SELECT a.demographic_no, a.appointment_date 
				FROM appointment a, demographic d
				WHERE a.demographic_no = d.demographic_no
				AND d.provider_no = '${provider}'
				AND DATE(a.appointment_date) >= ${upperLimit.date}
				AND a.status != 'D' AND a.status != 'C' AND a.status != 'N'
				AND a.demographic_no > 0
				GROUP BY a.demographic_no HAVING COUNT(a.demographic_no) > -1
			) app2
			ON (d.demographic_no = app2.demographic_no)
			
			-- WITHOUT a billing for Q131A in the last 24 months
			LEFT JOIN (
				SELECT boc.id, boc.demographic_no
				FROM billing_on_cheader1 boc, billing_on_item boi
				WHERE boc.id = boi.ch1_id 
				AND boc.status != 'D'
                                AND boi.status != 'D'
				AND boi.service_code = ${servicecode1}
				AND boi.service_date >= ${lowerLimit.date}
				GROUP BY boc.demographic_no 
			) BILL1
			ON (d.demographic_no = BILL1.demographic_no)
			
			 LEFT JOIN (
                                SELECT boc.id, boc.demographic_no, MAX(boi.service_date) AS billing_date
                                FROM billing_on_cheader1 boc, billing_on_item boi
                                WHERE boc.id = boi.ch1_id
                                AND boc.status != 'D'
                                AND boi.status != 'D'
                                AND boi.service_code = ${servicecode1}
                                GROUP BY boc.demographic_no
                        ) BILL1_0
                        ON (d.demographic_no = BILL1_0.demographic_no)

			-- WITHOUT a service code of Q141A billed
			LEFT JOIN (
				SELECT boc.id, boc.demographic_no, MAX(boi.service_date) AS billing_date
				FROM billing_on_cheader1 boc, billing_on_item boi
				WHERE boc.id = boi.ch1_id 
				AND boc.status != 'D'
                                AND boi.status != 'D'
				AND boi.service_code = ${servicecode2}
				GROUP BY boc.demographic_no 
			) BILL2
			ON (d.demographic_no = BILL2.demographic_no)
			

			WHERE d.patient_status = ${pstatus}
			AND d.provider_no = '${provider}'
			AND d.sex = ${gender}
			AND d.demographic_no > 0 
			AND FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,"-",d.month_of_birth,"-",d.date_of_birth) ), ${upperLimit.date} ) / 365.25 ) ) 
				BETWEEN ${lowerLimit.age} AND ${upperLimit.age}
                        AND MAM.id IS NULL AND BILL1.id IS NULL AND BILL2.id IS NULL AND MAM_i.id IS NULL AND dxr.dxresearch_no IS NULL

			GROUP BY d.demographic_no HAVING COUNT(d.demographic_no) > -1
		</query>
	</drillDownQuery>
	<shared>false</shared>
	<sharedMetricSetName>OntarioMD Breast Cancer Screening</sharedMetricSetName>
	<sharedMetricDataId>Status</sharedMetricDataId>
	<sharedMappings>
		<sharedMapping fromLabel="% Mammogram Done" toLabel="Screening up to date"/>
		<sharedMapping fromLabel="% Not Done" toLabel="Screening overdue"/>
		<sharedMapping fromLabel="% Excluded" toLabel="Excluded"/>
	</sharedMappings>
</indicatorTemplateXML>
