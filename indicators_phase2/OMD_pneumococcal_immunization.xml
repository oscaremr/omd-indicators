<?xml version="1.0" encoding="UTF-8"?>
<indicatorTemplateXML>
	<author>Colcamex Resources Inc. for Oscar EMR</author>
	<uid></uid>
	<heading>
		<category>Preventive Health Care</category>
		<subCategory>OMD-Immunization</subCategory>
		<name>OMD-Pneumococcal Immunization</name>
		<metricSetName>OntarioMD Pneumococcal Immunization</metricSetName>
		<metricLabel></metricLabel>
		<definition>% of patients aged 65 and older who have a record of receiving a pneumococcal vaccine</definition>
		<framework>Based on and adapted from HQO's PCPM: Priority Measures for System and Practice Levels (Oct 2015</framework>
		<frameworkVersion>02-02-2018</frameworkVersion>
		<notes></notes>
	</heading>
	<indicatorQuery>
		<version>02-02-2018</version>
		<params>
			<!-- 
				Required parameter provider. Value options are: 
					[ an individual providerNo, or provider range ] ie: value="370, 1001" 
					"all" ie: value="all" including null.
					"loggedInProvider" ie:
				Default is "loggedInProvider"
				Use this parameter in the query as ${provider}
				This parameter should be used for fetching patient's assigned to a MRP.
				ie: WHERE demographic.provider_no = ${provider}
			 Pneumovax = Pneu-P-23,  Pneu-C = Pneu-C-13
			-->
			<parameter id="provider" name="provider_no" value="loggedInProvider" />
			<parameter id="prevention" name="Immunization" value="'Pneumovax'" />	
			<parameter id="pneu-c" name="pneu-c" value="'Pneu-C%'" />

			<parameter id="pstatus" name="PatientStatus" value="'AC'" />	
		</params>
		<range>
			<lowerLimit id="age" label="Min Patient Age" name="Age" value="65" />
			
			<lowerLimit id="date12" label="12 Months" name="Date12" value="DATE_SUB( NOW(), INTERVAL 12 MONTH )" />
		</range>
		<query>
			<!-- Indicator SQL Query here -->
			SELECT
				IF ( COUNT(fin.patient) > 0,
					SUM( IF(fin.pneu > 0, 1, 0) ) 
				,0 ) AS "% Has Pneumococcal Vaccine",
			
				IF ( COUNT(fin.patient) > 0,
					SUM( IF( fin.pneu IS NULL AND fin.pneu2 IS NULL , 1, 0 ) )					 
				,0 ) AS "% No Pneumococcal Vaccine",
			
				IF ( COUNT(fin.patient) > 0,
					SUM( IF( fin.pneu IS NULL AND fin.pneu2 > 0, 1, 0 ) )					
				,0 ) AS "% Pneumococcal refused"
			FROM (
			
				SELECT 
					d.demographic_no AS patient,
					PNEU.id as pneu,
					PNEU2.id as pneu2
				FROM demographic d
			
				-- LOOK FOR pneumococcal VACCINES
				LEFT JOIN ( 
					SELECT p.demographic_no, p.id
					FROM preventions p	
					WHERE ( p.prevention_type = ${prevention} OR p.prevention_type LIKE ${pneu-c} )					
					AND p.deleted = 0
					AND p.refused = 0
					AND p.demographic_no > 0
					GROUP BY p.demographic_no HAVING COUNT(p.demographic_no) > -1
				) PNEU 
				ON (d.demographic_no = PNEU.demographic_no )
				
				
				-- LOOK FOR refused pneumococcal VACCINES
				LEFT JOIN ( 
					SELECT p.demographic_no, p.id, MAX(p.prevention_date) AS prevention_date
					FROM preventions p	
					WHERE ( p.prevention_type = ${prevention} OR p.prevention_type LIKE ${pneu-c} )					
					AND p.deleted = 0
					AND p.refused = 1
					AND p.demographic_no > 0
					AND p.prevention_date >= ${lowerLimit.date12}
					GROUP BY p.demographic_no HAVING COUNT(p.demographic_no) > -1
				) PNEU2
				ON (d.demographic_no = PNEU2.demographic_no )
				
				WHERE d.patient_status = ${pstatus} 
				AND d.provider_no = '${provider}'
				AND d.demographic_no > 0 
				AND ( FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,"-",d.month_of_birth,"-",d.date_of_birth) ), 
					NOW() ) ) / 365.25 ) >= ${lowerLimit.age} )
			) fin
		</query>
	</indicatorQuery>
	<drillDownQuery>
		<version>02-02-2018</version>
		<params>
			<parameter id="provider" name="provider_no" value="loggedInProvider" />
			<parameter id="prevention" name="Immunization" value="'Pneumovax'" />
			<parameter id="pneu-c" name="pneu-c" value="'Pneu-C%'" />

			<parameter id="pstatus" name="PatientStatus" value="'AC'" />
			<parameter id="sharedMetricLabel" name="sharedMetricLabel" value="sharedMetricLabel" />
		</params>
		<range>
			<lowerLimit id="age" label="Min Patient Age" name="Age" value="65" />
			
			<lowerLimit id="date12" label="12 Months" name="Date12" value="DATE_SUB( NOW(), INTERVAL 12 MONTH )" />
		</range>
		<displayColumns>
			<column id="demographic" name="d.demographic_no" title="Patient Id" primary="true" />
			<column id="name" name="CONCAT( d.last_name, ', ', d.first_name )" title="Patient Name" primary="false" />
			<column id="dob" name="DATE_FORMAT( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth), '%m-%d-%Y' )" title="Patient DOB (mm-dd-yyyy)" primary="false" />
			<column id="age" name="FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth) ), NOW() ) ) / 365.25 )" title="Patient Age" primary="false" />
			<column id="phonenumber" name="d.phone" title="Patient Phone Number" primary="false" />
			
			<column id="refused" name="IFNULL(PNEU2_0.refused, 'No')" title="Refusal Status" primary="false" />
			<column id="refusedDate" name="IFNULL( DATE_FORMAT( DATE(PNEU2_0.prevention_date), '%m-%d-%Y' ) , '')" title="Date of Most Recent Refusal (mm-dd-yyyy)" primary="false" />
			<column id="pneuC_Date" name="IFNULL( DATE_FORMAT( DATE(PNEU_C.prevention_date), '%m-%d-%Y' ) , '')" title="Date of Most Recent Pneu-C-13 (mm-dd-yyyy)" primary="false" />
			<column id="pneuP_Date" name="IFNULL( DATE_FORMAT( DATE(PNEU_P.prevention_date), '%m-%d-%Y' ) , '')" title="Date of Most Recent Pneu-P-23 (mm-dd-yyyy)" primary="false" />
		</displayColumns>
		<exportColumns>
			<column id="demographic" name="d.demographic_no" title="Patient Id" primary="true" />
			<column id="firstName" name="d.first_name" title="First Name" primary="false" />
			<column id="lastName" name="d.last_name" title="Last Name" primary="false" />
			<column id="dob" name="DATE_FORMAT( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth), '%m-%d-%Y' )" title="Patient DOB (mm-dd-yyyy)" primary="false" />
			<column id="age" name="FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth) ), NOW() ) ) / 365.25 )" title="Patient Age" primary="false" />
			<column id="phonenumber" name="d.phone" title="Patient Phone Number" primary="false" />
				
			<column id="refused" name="IFNULL(PNEU2_0.refused, 'No')" title="Refusal Status" primary="false" />
                        <column id="refusedDate" name="IFNULL( DATE_FORMAT( DATE(PNEU2_0.prevention_date), '%m-%d-%Y' ) , '')" title="Date of Most Recent Refusal (mm-dd-yyyy)" primary="false" />
                        <column id="pneuC_Date" name="IFNULL( DATE_FORMAT( DATE(PNEU_C.prevention_date), '%m-%d-%Y' ) , '')" title="Date of Most Recent Pneu-C-13 (mm-dd-yyyy)" primary="false" />
                        <column id="pneuP_Date" name="IFNULL( DATE_FORMAT( DATE(PNEU_P.prevention_date), '%m-%d-%Y' ) , '')" title="Date of Most Recent Pneu-P-23 (mm-dd-yyyy)" primary="false" />

		</exportColumns>
		<query>
			<!-- Drilldown SQL Query here -->
			SELECT 
			*	
			FROM demographic d
			
			-- LOOK FOR pneumococcal Pneu-P-13 VACCINES
			LEFT JOIN ( 
				SELECT p.demographic_no, p.id, MAX(p.prevention_date) AS prevention_date
				FROM preventions p	
				WHERE p.prevention_type = ${prevention}
				AND p.deleted = 0
				AND p.refused = 0
				AND p.demographic_no > 0
				GROUP BY p.demographic_no HAVING COUNT(p.demographic_no) > -1
			) PNEU_P
			ON (d.demographic_no = PNEU_P.demographic_no )

			-- LOOK FOR pneumococcal Pneu-C-13 VACCINES
                        LEFT JOIN ( 
                                SELECT p.demographic_no, p.id, MAX(p.prevention_date) AS prevention_date
                                FROM preventions p    
                                WHERE p.prevention_type LIKE ${pneu-c} 
                                AND p.deleted = 0 
                                AND p.refused = 0 
                                AND p.demographic_no > 0 
                                GROUP BY p.demographic_no HAVING COUNT(p.demographic_no) > -1
                        ) PNEU_C
                        ON (d.demographic_no = PNEU_C.demographic_no )
		
			
			-- LOOK FOR refused pneumococcal VACCINES in last 12 months
			LEFT JOIN ( 
				SELECT p.demographic_no, p.id, MAX(p.prevention_date) AS prevention_date
				FROM preventions p	
				WHERE ( p.prevention_type = ${prevention} OR p.prevention_type LIKE ${pneu-c} )
				AND p.deleted = 0
				AND p.refused = 1
				AND p.demographic_no > 0
				AND p.prevention_date >= ${lowerLimit.date12}
				GROUP BY p.demographic_no HAVING COUNT(p.demographic_no) > -1
			) PNEU2			
			ON (d.demographic_no = PNEU2.demographic_no )
			
			 -- LOOK FOR refused pneumococcal VACCINES
                        LEFT JOIN (
                                SELECT p.demographic_no, p.id, MAX(p.prevention_date) AS prevention_date, 'Yes' AS refused
                                FROM preventions p
                                WHERE ( p.prevention_type = ${prevention} OR p.prevention_type LIKE ${pneu-c} )
                                AND p.deleted = 0
                                AND p.refused = 1
                                AND p.demographic_no > 0
                                GROUP BY p.demographic_no HAVING COUNT(p.demographic_no) > -1
                        ) PNEU2_0
                        ON (d.demographic_no = PNEU2_0.demographic_no )

			WHERE d.patient_status = ${pstatus} 
			AND d.provider_no = '${provider}'
			AND d.demographic_no > 0 
			AND ( FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,"-",d.month_of_birth,"-",d.date_of_birth) ), 
				NOW() ) ) / 365.25 ) >= ${lowerLimit.age} )
				<!--
			AND IF( "${sharedMetricLabel}" = "Pneumococcal received", PNEU.id > 0,  
			    IF( "${sharedMetricLabel}" = "Pneumococcal overdue", PNEU.id = 0, ( IF("${sharedMetricLabel}" = "Pneumococcal refused", PNEU2.id > 0, 1=1)) ) )
		-->
		</query>
	</drillDownQuery>
	<shared>true</shared>
	<sharedMetricSetName>OntarioMD Pneumococcal Immunization</sharedMetricSetName>
	<sharedMetricDataId>Status</sharedMetricDataId>
	<sharedMappings>
		<sharedMapping fromLabel="% Has Pneumococcal Vaccine" toLabel="Pneumococcal received"/>
		<sharedMapping fromLabel="% No Pneumococcal Vaccine" toLabel="Pneumococcal overdue"/>
		<sharedMapping fromLabel="% Pneumococcal refused" toLabel="Pneumococcal refused"/>
	</sharedMappings>
</indicatorTemplateXML>
