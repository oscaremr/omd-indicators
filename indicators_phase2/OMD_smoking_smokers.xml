<?xml version="1.0" encoding="UTF-8"?>
<indicatorTemplateXML>
	<author>Trinity Healthcare Technologies for Oscar EMR</author>
	<uid></uid>
	<heading>
		<category>Preventive Health Care</category>
		<subCategory>OMD-OntarioMD Smoking Status</subCategory>
		<name>OMD-Smoking Status properly recorded</name>
		<metricSetName>OntarioMD Smoking Status</metricSetName>
                <metricLabel>Smokers</metricLabel>

		<definition>% of patients >= 12 years old for whom smoking status is recorded</definition>
		<framework>Based on and adapted from AFHTO D2D 3.0 Indicators: Data Dictionary version 4 (Nov 2015)</framework>
		<frameworkVersion>02-15-2018</frameworkVersion>
		<notes></notes>
	</heading>
	<indicatorQuery>
		<version>02-15-2018</version>
		<params>
			<!-- 
				Required parameter provider. Value options are: 
					[ an individual providerNo, or provider range ] ie: value="370, 1001" 
					"all" ie: value="all" including null.
					"loggedInProvider" ie:
				Default is "loggedInProvider"
				Use this parameter in the query as ${provider}
				This parameter should be used for fetching patient's assigned to a MRP.
				ie: WHERE demographic.provider_no = ${provider}
			SmkC : stop smoking in which year
			SmkS: start smoking in which year
			SMK: never smoke , the dataField in measurement is "Never",  "Former", "Current"
			-->
			<parameter id="provider" name="provider_no" value="loggedInProvider" />
			<parameter id="pstatus" name="Patient Status" value="'AC'" />
<!--
			<parameter id="smokingstart" name="Start Smoking in Measurement" value="'SmkS'" />		
			<parameter id="smokingstop" name="Stop Smoking in Measurement" value="'SmkC'" />		
			<parameter id="smokingstatus" name="Smoking Status in Measurement" value="'SMK'" />		
			<parameter id="smokingperday" name="Smoking packs per day in Measurement" value="'SmkD'" />		
			<parameter id="smoking" name="Smoking in Measurement" value="'Smk%'" />		
-->
			<parameter id="smokingstatus" name="Smoking Status in Intake (Current, Former, Never)" value="'SMK'" />
			<parameter id="smokingperday" name="Smoking packs per day in intake" value="'SmkD'" />		
			<parameter id="smokingstatus_cdm" name="Smoking Status in CDM indicator (yes/no)" value="'SKST'" />
			<parameter id="smokingperday_cdm" name="Smoking packs per day in CDM indicator" value="'POSK'" />		
			<parameter id="dxcode" name="Disease Registry Code" value="'3051'" />		
		</params>

		<range>
			<lowerLimit id="age" label="Lowest Age" name="Age" value="12" />
		</range>
		<query>
			<!-- Indicator SQL Query here -->
			SELECT

				IF( COUNT(fin.patient) > 0, 
                                         SUM( IF( fin.dx IS NULL AND (fin.status = 'Never' OR fin.status = 'Former' OR fin.status = 'No'), 1, 0) )
				, 0 ) AS "% Non-smokers",
			
				IF( COUNT(fin.patient) > 0, 
					SUM( IF( fin.status = 'Current' OR fin.status = 'Yes' OR fin.dx > 0, 1, 0) ) 
				, 0 ) AS "% Smokers",

				IF( COUNT(fin.patient) > 0,
                                        SUM( IF( fin.status IS NULL AND fin.dx IS NULL, 1, 0) )     
                                , 0 ) AS "% Not documented"
			
			FROM (
				SELECT
					d.demographic_no AS patient,
					dxr.dxresearch_no as dx,
					SM.dataField as status

				FROM demographic d
				LEFT JOIN (
					SELECT dxresearch_no, demographic_no
                                        FROM dxresearch
                                        WHERE demographic_no>0
                                        AND status = 'A'
                                        AND dxresearch_code = ${dxcode}
					AND coding_system = 'icd9'
                                        GROUP BY demographic_no HAVING COUNT(demographic_no) > -1
				) dxr 
                                ON d.demographic_no = dxr.demographic_no
	
				--Never smoking
				LEFT JOIN (
                                        SELECT m1.id, m1.dataField, m1.demographicNo, m1.dateObserved
                                        FROM measurements m1
                                        RIGHT JOIN (
						SELECT demographicNo, MAX(dateObserved) as dateObserved
                                                FROM measurements
                                                WHERE ( type = ${smokingstatus} OR type = ${smokingstatus_cdm} )
                                                AND demographicNo > 0
                                                GROUP BY demographicNo
                                        ) m2
					ON m1.dateObserved = m2.dateObserved
	                                AND m1.demographicNo = m2.demographicNo
					AND ( type = ${smokingstatus} OR type = ${smokingstatus_cdm} )
					ORDER BY m1.id DESC
                                ) SM 
                                ON (d.demographic_no = SM.demographicNo)


				WHERE d.patient_status = ${pstatus}
				AND d.provider_no = '${provider}'
				AND ROUND( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,"-",d.month_of_birth,"-",d.date_of_birth) ), NOW() ) / 365.25 ) ) >= ${lowerLimit.age} 
			) fin
		</query>
	</indicatorQuery>
	<drillDownQuery>
		<version>02-15-2018</version>
		<params>
			<parameter id="provider" name="provider_no" value="loggedInProvider" />
			<parameter id="pstatus" name="Patient Status" value="'AC'" />
			<parameter id="smokingstatus" name="Smoking Status in Intake (Current, Former, Never)" value="'SMK'" />
			<parameter id="smokingperday" name="Smoking packs per day in intake" value="'SmkD'" />		
			<parameter id="smokingstatus_cdm" name="Smoking Status in CDM indicator (yes/no)" value="'SKST'" />
			<parameter id="smokingperday_cdm" name="Smoking packs per day in CDM indicator" value="'POSK'" />		
<!--
			<parameter id="smokingstart" name="Start Smoking in Measurement" value="'SmkS'" />		
			<parameter id="smokingstop" name="Stop Smoking in Measurement" value="'SmkC'" />		
			<parameter id="smokingstatus" name="Smoking Status in Measurement" value="'SMK'" />		
			<parameter id="smokingperday" name="Smoking packs per day in Measurement" value="'SmkD'" />		
			<parameter id="smoking" name="Smoking in Measurement" value="'Smk%'" />		
-->
			<parameter id="dxcode" name="Disease Registry Code" value="'3051'" />		

		</params>

		<range>
			<lowerLimit id="age" label="Lowest Age" name="Age" value="12" />
		</range>

		<displayColumns>
			<column id="demographic" name="d.demographic_no" title="Patient Id" primary="true" />
			<column id="name" name="CONCAT( d.last_name, ', ', d.first_name )" title="Patient Name" primary="false" />
			<column id="dob" name="DATE_FORMAT( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth), '%m-%d-%Y' )" title="Patient DOB (mm-dd-yyyy)" primary="false" />
			<column id="age" name="FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth) ), NOW() ) ) / 365.25 )" title="Patient Age" primary="false" />	
                        <column id="phonenumber" name="IFNULL( d.phone, '')" title="Patient Phone Number" primary="false" />
                        <column id="dxdate" name="IFNULL( DATE_FORMAT(dxr.start_date, '%m-%d-%Y' ), '')" title="Last Date of Dx Code 3051 (mm-dd-yyyy)" primary="false" />
<!--
                        <column id="lastsmokingstatus" name="IFNULL( SM.dataField, '' )" title="Last Intake Value of Do You Smoke Cigarettes" primary="false" />
                        <column id="lastsmokingstatusdate" name="IFNULL( DATE_FORMAT( SM.dateObserved, '%m-%d-%Y' ), '')" title="Last Intake Date (mm-dd-yyyy)" primary="false" />
                        <column id="lastsmokingperday" name="IFNULL( SMD.dataField, '' )" title="Last Intake Value of Packs of Cigarettes Smoked per Day" primary="false" />
                        <column id="lastsmokingstatusdate" name="IFNULL( DATE_FORMAT( SMD.dateEntered, '%m-%d-%Y' ), '')" title="Last Intake Date (mm-dd-yyyy)" primary="false" />
-->
                        <column id="lastsmokingstatus" name="IFNULL( SM.dataField, '' )" title="Most Recently Recorded Value of SMK or SKST" primary="false" />
                        <column id="lastsmokingstatusdate" name="IFNULL( DATE_FORMAT( SM.dateObserved, '%m-%d-%Y' ), '')" title="Date of Most Recently Recorded Value of SMK or SKST (mm-dd-yyyy)" primary="false" />

		</displayColumns>

		<exportColumns>
			<column id="demographic" name="d.demographic_no" title="Patient Id" primary="true" />
			<column id="name" name="CONCAT( d.last_name, ', ', d.first_name )" title="Patient Name" primary="false" />
			<column id="dob" name="DATE_FORMAT( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth), '%m-%d-%Y' )" title="Patient DOB (mm-dd-yy)" primary="false" />
			<column id="age" name="FLOOR( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,'-',d.month_of_birth,'-',d.date_of_birth) ), NOW() ) ) / 365.25 )" title="Patient Age" primary="false" />	
                        <column id="phonenumber" name="IFNULL( d.phone, '')" title="Patient Phone Number" primary="false" />
                        <column id="dxdate" name="IFNULL( DATE_FORMAT(dxr.start_date, '%m-%d-%Y' ), '')" title="Last Date of Dx Code 3051 (mm-dd-yyyy)" primary="false" />
<!--
                        <column id="lastsmokingstatus" name="IFNULL( SM.dataField, '' )" title="Last Intake Value of Do You Smoke Cigarettes" primary="false" />
                        <column id="lastsmokingstatusdate" name="IFNULL( DATE_FORMAT( SM.dateObserved, '%m-%d-%Y' ), '')" title="Last Intake Date (mm-dd-yyyy)" primary="false" />
                        <column id="lastsmokingperday" name="IFNULL( SMD.dataField, '' )" title="Last Intake Value of Packs of Cigarettes Smoked per Day" primary="false" />
                        <column id="lastsmokingstatusdate" name="IFNULL( DATE_FORMAT( SMD.dateEntered, '%m-%d-%Y' ), '')" title="Last Intake Date (mm-dd-yyyy)" primary="false" />
-->
                        <column id="lastsmokingstatus" name="IFNULL( SM.dataField, '' )" title="Most Recently Recorded Value of SMK or SKST" primary="false" />
                        <column id="lastsmokingstatusdate" name="IFNULL( DATE_FORMAT( SM.dateObserved, '%m-%d-%Y' ), '')" title="Date of Most Recently Recorded Value of SMK or SKST (mm-dd-yyyy)" primary="false" />

		</exportColumns>

		<query>
			<!-- Drilldown SQL Query here -->
			SELECT
			*
			FROM demographic d
		

			 LEFT JOIN (
                                SELECT dxresearch_no, demographic_no, max(start_date) as start_date
                                        FROM dxresearch
                                        WHERE demographic_no>0
					AND status = 'A'
                                        AND dxresearch_code = ${dxcode}
					AND coding_system = 'icd9'
                                        GROUP BY demographic_no HAVING COUNT(demographic_no) > -1
                        ) dxr
                        ON d.demographic_no = dxr.demographic_no

                        --Never smoking
			LEFT JOIN (
                                SELECT m1.id, m1.dataField, m1.demographicNo, m1.dateObserved
                                FROM measurements m1
                                RIGHT JOIN (
                                        SELECT demographicNo, MAX(dateObserved) as dateObserved
                                        FROM measurements
                                        WHERE ( type = ${smokingstatus} OR type = ${smokingstatus_cdm} )
                                        AND demographicNo > 0
                                        GROUP BY demographicNo
                                ) m2
                                ON m1.dateObserved = m2.dateObserved
                                AND m1.demographicNo = m2.demographicNo
                                AND ( type = ${smokingstatus} OR type = ${smokingstatus_cdm} )
                                ORDER BY m1.id DESC
                        ) SM
                        ON (d.demographic_no = SM.demographicNo)

			--Smoking packs per day
--			 LEFT JOIN (
 --                               SELECT m1.id, m1.dataField, m1.demographicNo, m1.dateEntered
  --                              FROM measurements m1
   --                             INNER JOIN (
    --                                    SELECT MAX(id) AS id
     --                                   FROM measurements
      --                                  WHERE ( type = ${smokingperday} OR type = ${smokingperday_cdm} )
       --                                 AND demographicNo > 0
        --                                GROUP BY demographicNo
         --                       ) m2
          --                      ON (m1.id = m2.id)
           --             ) SMD
            --            ON (d.demographic_no = SMD.demographicNo)
	
			WHERE d.patient_status = ${pstatus}
			AND d.provider_no = '${provider}'
                        AND ( SM.dataField = 'Current' OR SM.dataField = 'Yes' OR dxr.dxresearch_no > 0 )

			AND ROUND( ABS( DATEDIFF( DATE( CONCAT(d.year_of_birth,"-",d.month_of_birth,"-",d.date_of_birth) ), NOW() ) / 365.25 ) ) >= ${lowerLimit.age}	
		</query>
	</drillDownQuery>
	<shared>false</shared>
        <sharedMetricSetName>OntarioMD Smoking Status</sharedMetricSetName>
        <sharedMetricDataId>Status</sharedMetricDataId>
        <sharedMappings>
                <sharedMapping fromLabel="% Non-smokers" toLabel="Non-smokers"/>
                <sharedMapping fromLabel="% Smokers" toLabel="Smokers"/>
                <sharedMapping fromLabel="% Not documented" toLabel="Not documented"/>
        </sharedMappings>

</indicatorTemplateXML>
